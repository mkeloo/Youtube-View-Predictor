All cards are in nm/mint condition.  Most have not been played with and those that have were sleeved and carefully shuffled to avoid damage.

All ultra rares are $5 each except for those listed below:
Registeel EX - $10
Victini Full Art - $10
Entei EX - $10

Code Cards: 381* - $85
* There are 381, but at one point my brother decided to play a "joke" on me and mix in some of his used cards with my un-used ones. I think I managed to pull them all out, but I would only count on 375-ish working. Luckily I know they were all from the first three or four sets that had code cards in them.

Professor Deck Box - $10 each
Professor Sleeves - $20 each
Celebi Sleeves - $15 each
Prerelease Deck Boxes - $5

Set: Rarity = # (how many of those are RH) / Rarity... etc/ Total = #

Lot #1 - $50
HP: C = 8/ U = 3 / Total = 11
PK: C = 1 / Total = 1
DF: C = 23 / U = 2 / Total = 25
CG: C = 4 / Total = 4
DP: C = 62 / U = 26/ R = 2 / Total = 90
MT: C = 46 (2)/ U = 23 (1)/ R = 7 / Total = 76
SW: C = 132 (2)/ U = 66/ R = 7(1)/ H = 3 / Total = 208

Lot #2 - $45
GE: C= 66/ U = 32/ R = 3/ H = 3 / Total = 104
MD: C = 65/ U = 39 (3)/ R = 3 / Total = 107
LA: C= 94 / U = 51/ R = 12 (6) H= 1 / Total = 158

Lot #3 - $110
SF: C = 129/ U = 74 (5)/ R = 9/ Total = 212
PL: C = 200 (1)/ U = 106 (5)/ R = 7/ H = 2 / Total = 315
RR: C = 248 (3)/ U = 134 (11)/ R = 14 (1)/ H = 3/ Total = 399

Lot #4 - $40
SV: C = 164 (1)/ U = 72 (2)/ R = 3 (1)/ H = 3 (1)/ Total = 242
AR: C = 61/ U = 36 (3)/ R = 6 (3)/ H = 1/ Total = 104

Lot #5 - $170
HGSS: C = 104/ U = 64 (11)/ R = 6 (1)/ H = 2/ Total = 176
UL: C = 129 (1)/ U = 72 (5)/ R = 10 (3)/ H = 4 (1)/ Total = 215
UD: C = 130 (1)/ U = 79 (6)/ R = 15/ H = 4 (2)/ Total = 228
TR: C = 308 (7)/ U = 174 (9)/ R = 30 (1)/ H = 8/ Total = 520
CL: C = 74 (1)/ U = 34/ R = 4/ H = 1/ Total = 113

Lot #6 - $200
BW: C = 409 (16)/ U = 262 (23)/ R = 45 (3)/ H = 8 (2)/ Total = 724
EP: C = 411 (7)/ U = 244 (15)/ R = 41 (2)/ H = 11 (2)/ Total = 707

Lot #7- $185
NV: C= 370 914)/ U = 199 (8)/ R = 39 (5)/ H = 14 94)/ Total = 622
ND: C = 76 (2)/ U = 41 (2)/ R = 3/ H = 3/ Total = 123
DaE: C = 344 (3)/ U = 183 (3)/ R = 28 (1)/ H = 9 (1)/ Total = 564

Lot #8- $135
DrE: C = 354 (2)/ U = 200 (4)/ R = 32 (2)/ H = 7 (1)/ Total =  593
BC: C = 236 (1)/ U = 128 (3)/ R = 23 (1)/ Total = 387
PS: C = 65/ U = 14/ R = 2/ Total = 81
PF: C = 146/ U = 71/ R = 9/ H = 3 (1)/ Total = 229
PB: C = 65/ U = 30/ R = 1/ Total = 96

Lot #9 - $85
LT: C = 237 (3)/ U = 98 (3)/ R = 39/ H = 16 (3)/ Total = 390
RC: C = (55)/ U = (36)/ Total = 91
