Please Like, Watch In HD, Favorite, and Subscribe. I Finally got a Safetran Type 1 Electronic Bell Crossing on Florin Road #1 and Union Pacific on The Sacramento Subdivision. It took me 5 times to come out to this crossing and finally get a train. Union Pacific trains didn't really use this line that much after 2009. BNSF mostly used it even though Union Pacific owned it. But I was Lucky to get Union Pacific on its line. The Union Pacific trains mostly use the fresno sub. This train is the ZLCBR that started back using the Sac Sub in 2010. More Info Below.

Crossing Info:
1 track, 6 signals, 4 gated, 2 gateless, 1 Harmon Cantilever, 1 Safetran Type 1 E-Bell, 1 WCH type 1.5 E-Bell, and 1 WCH type 2 E-Bell. Cantilever signal by me has Harmon 12" LEDs inside Harmon frames and brackets and Safetran 12" Fading LEDs inside Safetran frames and brackets back lights, WCH Gate Lights, Safetran EM port, and Safetran type 1 e-bell. Median signal on my side of track has Safetran 12" Fading LEDs inside Safetran frames and brackets, WCH Gate Lights, and EM Safetran port. gateless signal on my side of track has Harmon 12" LEDs inside Harmon frames and brackets, Harmon signal base, and WCH Type 2 e-bell. Gateless signal on other side has GS 12" LEDs inside Harmon frames and brackets and WCH signal base. Median signal on other side has Safetran 12" Fading LEDs inside Safetran frames and brackets, and WCH Gate Lights, Safetran EM port. Signal on other side and left side of the street has Harmon 12" Fading LEDs inside Harmon frames and WRRS brackets, Safetran Gate Lights, WCH gate mechanism, WCH signal base, and WCH Type 1.5 E-Bell.

Locomotives: 
UP #7676
UP #8735
UP #4048
UP #5095

Direction:
Northbound

Train Lines: 
Union Pacific Sacramento Subdivision and SACRT Blue Line overcrossing.

Crossing Company Tags: Union Pacific, Harmon, Western Cullen Hayes, Safetran, General Signal, Western Railroad Supply