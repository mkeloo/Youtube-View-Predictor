CINEwear Labs is a Fashion Consumer Electronics Company. We design and market our patented CINEshades stealth video sunglass system. The First and Only system that transforms CINEshades fashion sunglasses into lightweight stealth video eyewear that lets you privately watch TV and Movies from your smart phone or media player in 2D or 3D on a 75" virtual big screen without looking conspicuous!  

Check it out and "Like" us on Facebook to stay current on everything Nabes.

http://www.cinewearlabs.com/