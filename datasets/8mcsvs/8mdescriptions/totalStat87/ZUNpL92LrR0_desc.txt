Employment Opportunites

  We are now hiring due to an increase in growth
  Earn extra income through bonuses, commissions, and contests
  License fees reimbursed by Massage Retreat & Spa
  Team atmosphere focusing on Therapist's potential and longevity
  Expanding locations

Massage Retreat & Spa offers relaxing and rewarding Career Opportunities to Massage Therapists, Skin Therapists/Estheticians, Managers and Customer Service professionals.

Our employees truly enjoy working at Massage Retreat & Spa. "Massage Retreat & Spa is busy and the customers are great, I get treated with respect and I like the fact the I have opportunities to continue to learn and grow in my work."


As a Massage Therapist, you'll have the opportunity to work with a variety of clients in a calm, soothing, clean and warm environment. We provide everything from linens, tables and lotions/oils. Flexible scheduling is available. This is a perfect opportunity for someone looking to utilize their education/training as a "Career" therapist, or as supplemental income in a part-time capacity. Massage Therapists earn base verse commission pay plus tips, bonuses and contest incentives for re-booking, memberships, and product sales.

Our Skin Therapists/Estheticians perform first class services and procedures with an emphasis on facials, waxing, eyelash/eyebrow tinting. Top of the line, dermalogica Skin Care products are used. Flexible scheduling is available. Skin Therapists/Estheticians earn base pay and commissions on memberships, re-booking, product sales, and bonuses for monthly contests.

Our Desk Sales/Service Providers are our frontline with the customers. They perform a myriad of duties, including greeting customers, booking appointments, handling payments, and selling memberships. They are well-versed on our products and services, have proficient computer skills and present a clean, polished, professional image to our customers. Service desk personnel are paid competitively, and work together as a team.

Managers are professionals with experience in managing people and leading a team of massage and skin therapists along with customer service team. This position operates the store in all facets from sales, operations and all customer service issues.


http://massageretreat.com/careers.html