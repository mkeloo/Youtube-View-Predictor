Homosassa Springs Wildlife State Park has a permanent population of mantaees, but the population increases in the winter months. This herd was approx. 50-60 animals. 
You are not allowed to bother them, but they are very curious when active so they interact with you!!
They seem to enjoy being petted and will even roll over on their backs so you can get under their flippers! Like a 1 ton puppy dog!