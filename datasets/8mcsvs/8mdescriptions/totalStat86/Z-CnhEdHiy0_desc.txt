Chris De La Rosa of CaribbeanPot.com kicks of his annual July Month of Grilling with this scrumptious Mango Coconut Shado Beni (chadon beni or culantro) Grilled chicken. A wicked marinade made up of coconut milk, freshly grated ginger, organic honey, limes, ripe mango, shado beni and  Caribbean sunshine - scotch bonnet pepper is used to infuse the chicken piece with true Caribbean flavors. Before they are grilled on an open flame to perfection. If you cannot get Shado beni, you can use cilantro with the same flavor profiles.


For this grilled chicken recipe you'll need:

3 chicken breasts (about 2.5 - 3 lbs)

1 large mango (diced)
1/2 cup chopped shado beni (or 1 cup cilantro)
2 scallions
2 sprigs thyme
1 tablespoon grated ginger
4 cloves garlic
1 cup coconut milk
2 tablespoon olive oil
2 limes (juice)
1/2 scotch bonnet pepper (no seeds)
2 tablespoon honey
1 cup water (divided)
1/4 teaspoon sea salt


More Caribbean recipes can be found at http://www.caribbeanpot.com

Get my latest cookbook, The Vibrant Caribbean Pot - 100 Traditional And Fusion Recipes Vol 2 @ http://www.CaribbeanPot.com/book/ or Amazon @ http://www.amazon.ca/Vibrant-Caribbean-Traditional-Fusion-Recipes/dp/0992050502

Connect with Chris De La Rosa

Facebook: http://www.facebook.com/pages/Caribbe...
Twitter: https://twitter.com/obzokee
Instagram: caribbeanpot
Contact: http://caribbeanpot.com/contact/
Pinterest: https://pinterest.com/caribbeanpot/th...

To learn more about Chris De La Rosa, you can visit http://www.ChrisDeLaRosa.com