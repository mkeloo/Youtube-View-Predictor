I arranged the instrumental using digital composition for better quality and flexibility. I prefer it this way, especially when covering tracks from video games; the limits are truly endless! (Gah, YouTube really loves to muck up the synching...) 

The Final Fantasy series is definitely one of my favourites. I just love the holy quality of this particular song, and decided to integrate the Mt. Gagazet (People of the North Pole) theme into this recording. I hope it turned out well! =(^o~)=

LYRICS ~
Ieyui
Nobomeno
Renmiri
Yojuyogo
Hasatekanae
Kutamae

MP3 DOWNLOAD LINK ~
http://www.mediafire.com/?f7e2fn2d24co92a

CREDITS ~
Original Composers: Masashi Hamauzu & Nobuo Uematsu
Lyrics: Kazushige Nojima
Arranged and Performed by Musical Moogle