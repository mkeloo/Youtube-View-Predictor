My personal Top 100 Sega songs, if you have a suggestion then toss one out since I may miss a couple. 

I will try to keep it to a minimum of 1 or 2 songs per game or series to prevent the list from being filled with Sonic and Streets of Rage songs.

#100 Fatal Labyrinth - Floors 1-9
#99 Super Hang-On - Sprinter
#98 Land Stalker - Lyle is Going
#97 Pengo - Popcorn
#96 Columns - Clotho
#95 Super Monkey Ball 2 - Dr. Bad-boon's Base
#94 Puyo Pop Fever - Taisen 4
#93 Let's Tap - 11 - Violet Interspace
#92 Feel the Magic XX/XY - Love Scene
#91 Alex Kidd in Miracle World - Castle Theme
#90 Chu Chu Rocket - Single Player Theme
#89 Sakura Teisan 3 - Flowery Paris
#88 X-Men 2 Clone Wars - Magneto's Quarters
#87 Virtual On - Into the Blue Sky
#86 Last Bronx - Jaggy Love
#85 Toejam & Earl - Toejam Slowjam
#84 Dynamite Headdy - Dark Demon's Song
#83 Comix Zone - Episode 3
#82 2Spicy- Mansion
#81 Gunstar Heroes - Bravoo Man
#80 Virtua Cop 2 - Dance of Death
#79 Virtua Racing Deluxe - Replay
#78 Altered Beast - Level 1
#77 Power Drift - Like the Wind
#76 Shining Force - Battle 3
#75 Fighting Vipers - Raxel
#74 Vectorman - Tidal Surge
#73 Ristar - Greedy's Game
#72 Knuckles Chaotix - Door into Summer
#71 Otogi 2: Immortal Warriors - Voice
#70 Fantasy Zone - Victory Way
#69 GunValkyrie - Naglfar's Pit
#68 Monster World 2 Ending Acoustic Mix
#67 Fighters Megamix - Ending A Light
#66 Samba de Amigo Ver 2000 - Vamos a Carnival
#65 Phantasy Star III - Journy Begins
#64 Golden Axe - Wilderness (ROUND1)
#63 Rent-A-Hero - Can You Become Rent-A-Hero
#62 Virtua Fighter 2 - Ride the Tiger
#61 Space Channel 5 - Evila
#60 Sonic R - Can you feel the Sunshine?
#59 Pole's Big Adventure - Jungle
#58 Sega Rally 2 - Searchin for my Dreams
#57 Space Harrier - Haya oh
#56 Sonic the hedgehog - Marble Zone
#55 Yakuza 2 - Block Head Boy
#54 Shining Force 3 - Soldier of the Republic
#53 Astal - River of Dreams
#52 House of the Dead 2 - Magician Battle
#51 Ecco Tides of Time - Tube of Medusa
#50 Sonic 2 - Chemical Plant Zone
#49 Crazy Taxi 2 - We're Back!
#48 Segata Sanshiro!
#47 Virtua Fighter 3 - Underground Sarah Stage
#46 Sonic Adventure - Crank the Heat
#45 Skies of Arcadia - Ramirez Theme
#44 Shinobi - Red Blade
#43 Daytona 2 - Skyscraper Sequence
#42 MadWorld - Look Pimpin!
#41 Shinobi - Terrible Beat
#40 Sonic CD Tidal Tempest Zone
#39 Thunderforce VI - KIN3 COOL BOSS 5A
#38 Shining the Holy Ark - Battle Theme
#37 Rez - Area 5 (Fear)
#36 Bayonetta - Mysterious Destiny
#35 Ikaruga - Chapter 4 (Reality)
#34 7th Dragon - Labyrinth (Jungle navigation)
#33 Outrun 2 - Shiny World
#32 Billy Hatcher - Tumbling Xylophone
#31 Afterburner Climax Theme
#30 Radiant Silvergun - Debris