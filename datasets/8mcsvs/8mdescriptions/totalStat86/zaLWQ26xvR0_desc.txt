Goodmorning loves! Vandaag is het tijd voor de vlog van afgelopen vrijdag. Ik werd wakker met Jiami en Sharon, en later op de dag vertrok ik naar Tilburg met Dwayne voor een overnachting bij Van der Valk te Gilze! Enjoy!

// VLOGIDAYS 2014 

Iedere dag om 6:00 een nieuwe vlog online! Vlogidays  begint op maandag 1 december 2014 en zal eindigen op zondag 4 januari 2015. Er zullen tussendoor zo nu en dan ook nog tutorials verschijnen! Ik film trouwens 3 dagen vooruit.

(Youtube kanaal van Dwayne: http://bit.ly/1rQi7d2) 

// TODAYSBEAUTY 
Vanaf nu iedere woensdag en zaterdag om 15:00 een nieuwe video (met uitzondering van december, dan komt er iedere dag een nieuwe video online). 

// SOCIAL MEDIA

B l o g : http://www.todaysbeauty.nl
E - m a i l : contact@todaysbeauty.nl 
I n s t a g r a m : todaysbeauty
F a c e b o o k : http://www.facebook.com/todaysbeauty/
B l o g l o v i n : http://www.bloglovin.com/blog/3992851...

// OVER MIJ

Mijn naam is Manon van Todaysbeauty. Ik post regelmatig nieuwe video's over beauty, make-up, fashion, lifestyle en de laatste fashion en beauty trends. Hierbij kun je denken aan leuke make-up tutorials, make-up reviews, outfits, fashion, make-up trends, fashion trends, shoplogs, en nog veel meer. Wil je altijd up-to-date blijven van mijn laatste filmpjes? Abonneer je dan op mijn kanaal, het is helemaal gratis, en zo ben jij altijd als eerste op de hoogte =)! Enjoy beauty's!