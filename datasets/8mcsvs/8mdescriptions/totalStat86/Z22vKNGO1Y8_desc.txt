Peter Hook & The Light perform Joy Division's 'She's Lost Control' live at Tele-Club in Ekaterinburg, Russia as part of the band's April 2013 Russian tour.

www.facebook.com/peterhookandthelight
www.peterhook.co.uk
www.twitter.com/peter_hook1
www.tele-club.ru