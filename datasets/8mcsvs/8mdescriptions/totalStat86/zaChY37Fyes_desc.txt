With 4 of us photographers combining our video photographic footage together, here is our first video of the Railroad Profile series. 

The first Railroad Profile that we look at is Amtrak Cascades because it is an intercity passenger train service that serves between Seattle, Portland, Vancouver BC, and all intermediate station stops. With the ridership skyrocketing from last year and with the increasing gas prices at the pumps, more people are taking Amtrak Cascades for having the convenience and the luxury of riding stress free. 

A round trip ticket on Amtrak Cascades between Seattle and Portland typically cost about $60-70 compared to airline ticket, which typically cost around $200-$300 for the same trip. Which one would you take?

I would like to say thank you to the following people who also contributed to the video: Dan Morris (youtube: css903), Jason Hill (youtube: kd7tqn), and Nick Dumas (youtube: fiddleman200616). And also be sure to checkout their channels for more great videos as well.

Since this is part 1 of the video, part 2 will introduce the tour inside Amtrak Cascades and the ride onboard an Amtrak Cascades train from Seattle to Portland. So stay tuned for Part 2 sometime this summer. Enjoy the video.