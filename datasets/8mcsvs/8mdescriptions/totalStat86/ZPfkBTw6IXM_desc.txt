An introduction to the shiniest planet in the solar system. Fun science now comes with 100% less theme tune (because I just plain forgot to record one). I'll be more vigilant next time.

Special thanks to Phil Plait for helping me make sure I wasn't getting any of my facts wrong: https://twitter.com/BadAstronomer