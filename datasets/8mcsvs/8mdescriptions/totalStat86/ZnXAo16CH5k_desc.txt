Creating his first ever fine jewellery piece, designer Gareth Pugh is working with Forevermark to bring the concept of Promise to life. So begins a journey that follows the path of a Forevermark diamond - from its origins in the earth right through to its eventual home in a truly exceptional, unique piece of art.

Find out more at www.forevermark.com/promise
#forevermarkpromise