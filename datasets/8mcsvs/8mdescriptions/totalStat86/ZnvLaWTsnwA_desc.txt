Le site web: http://www.bisse-du-tsittoret.ch

Rando suisse dans la rgion de Crans-Montana, en Valais. Le bisse du Tsittoret est un canal d'irrigation utilis afin d'acheminer l'eau depuis les glaciers jusqu'aux cultures. 

Hiking in Switzerland is just wonderful. This trekking in the region of Crans-Montana is called "bisse du tsittoret". From there you can appreciate a spectacular view on the swiss alps (matterhorn, weisshorn etc..) The nature of high altitude is also beautiful.

----
En partant de Vermala pour rejoindre la source de la Tiche, le bisse du Tsittoret est une des plus belles randonnes que vous puissiez faire dans la rgion de Crans-Montana en Valais. Tout en vous baladant le long du bisse, dcouvrez cette promenade au panorama poustouflant des plus beaux sommets des Alpes valaisannes. Une balade parmi les bisses du valais permet d'observer la flore et la faune de montagne.



----------------
Auteur: Sylvain Botter
Musique: Reid Willis



----------------------------------------------------------------------------------------------------------------

A wonderful hike in the swiss Alps along the "Bisse" of Tsittoret, a small water canal used in the Past Times to irrigate the fields in the mountains. If you are looking for things to do in Switzerland, this is a perfect destination for swiss holidays close to swiss ski resorts.

If you liked the video, please visit my portfolio website:
http://www.sylvainbotter.com