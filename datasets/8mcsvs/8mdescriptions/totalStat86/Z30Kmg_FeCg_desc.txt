Call of Duty World at War gameplay/commentary by Nicky. Playing Dome Team DeathMatch using the Type 100. Final Score 40-13.

Today I'm talking about a Black Ops Wishlist, written by Kyle, Sonya, and myself. Below is the complete list:

-The following are things I didn't get a chance to talk about in the video.-

Change Stopping Power Pro to be more useful. Usually when destroying vehicles, you're using Cold Blooded Pro.
Put zombies in different difficulties (to make it easier for less experienced players to get to higher levels, but limit difficulties to easy and normal).
Return of Pack a Punch.
Add weapon with unlimited ammo (like flamethrower) but make it stronger than the flamethrower.
Make party system better (sometimes the party is seperated and put in different lobbies).
Mute everyone except party members button.
Make a perk like Cold-Blooded Pro, invisible to killstreaks unlike Camo.
If Grenade Launchers are to be added, keep them like they were in COD5, not like in MW2.
Show coolest looking killcam, not last. Perhap the game could be able to detect a throwing knife kill and choose it over a Tar kill, ect. 
Don't drop a player late in a game, or if the team they are dropped on is losing by a certain amount of points. (20 kills into a FFA, 3 to 3 score in SND, 2000 to 7000 points in TDM, ect.)

-Everyting I talked about is briefly listed below-

No Commando
No Deathstreaks or extra points for killing people using deathstreaks. Also tune down painkiller.
Bring back the PPSH or similiar weapon.
Fix spawns.
Don't place player in the same game twice.
Assist points = damage given to enemy.
Killstreaks carry on.
No overpowered weapons.
No vehicles (unless killstreaks).
Make pick up gun button and plant/diffuse button different buttons.
Host Migrations.
Balance SMGs.
No hacking prestiges.
More rewards for prestiging. For example, weapons unlocked earlier each prestige.

Thank you Treyarch, if you do watch this video, for listening to your community to improve the Call of Duty series!

Thank you to all my other viewers too, from now on I'll be filming in HD!

Please subscribe for future commentaries!