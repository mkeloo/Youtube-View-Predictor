Here is a musical monorail trip around the Walt Disney World Resort set to the soundtrack of "Soarin Over California" by Jerry Goldsmith.  I filmed the scenes in 2003 during a week stay.

(C) 2003/2006 by Terrance Tucker