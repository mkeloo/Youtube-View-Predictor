Genk. Een creatieve, innoverende, dynamische stad. Een stad met alle troeven op tafel, maar ook met verborgen verleiders. Een toeristische groeipool met een heel eigen karakter. 

Je hoeft niet oud te zijn... om authentiek te zijn. Genk biedt verrassingen op elke hoek. Hier vind je groot genot in kleine dingen. In Genk ben je geen toerist. Hier ben je n van ons.