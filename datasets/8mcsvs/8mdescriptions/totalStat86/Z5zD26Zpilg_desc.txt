This little gadget is something I came up with to see if it would help stabilize my small kodak zi8.
The camera cradle is similar to the other commercial camera cradle or camera caddy that is sold on ebay for over $50.
You won't get a steadicam result with a camera cradle, but it will be a very comfortable way to grip the small camera. The main benefit is all of the neat ways you can add to or modify a cradle to make many useful attachments and gadgets you could not do unless you had a cradle.
It is a fun project that only takes a dollar worth of materials. The cradle shown was made in 15 minutes and is a rough prototype, it can be made to look much better. This was just to see how it would work.

If you like my DIY videos and want to subscribe, you can go to:

http://www.youtube.com/subscription_center?add_user=mrhulot101


  If you would like to check out my videos before subscribing first, you can go to my channel at

Robb's Home Made Life
http://www.youtube.com/user/mrhulot101?feature=mhee

 I add new videos every week. If you make DIY or outdoor or low cost living or prepper videos, I would be happy to check them out.