Neon Jungle's debut album Welcome To The Jungle is OUT NOW: http://smarturl.it/nj_albumitunes Including the new single Louder, plus Braveheart, Trouble & Welcome To The Jungle

Click To Subscribe: http://bit.ly/1giCswE 
Sign up to the official Neon Jungle mailing list: http://smarturl.it/NeonJungleMail 
Buy signed CDs and merch from Neon Jungle's official store: http://smarturl.it/NJ_Store

Follow Neon Jungle:
https://www.neon-jungle.com
https://www.facebook.com/NeonJungleMusic
https://twitter.com/NeonJungleMusic
https://soundcloud.com/neonjunglemusic
http://instagram.com/neonjunglemusic
http://smarturl.it/NJ_Gplus
http://smarturl.it/NJ_Spotify

Music video by Neon Jungle performing Sleepless in London. (C) 2014 Sony Music Entertainment UK Limited