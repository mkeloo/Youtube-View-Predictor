http://www.twitter.com/dreamzon
Use this video to help learn the dance for Crayon Pop dance cover!
[Crayon Pop Youtube Dance Cover Event for "FM"]

Crayon Pop holds an dance cover event on Youtube for "FM"!

- Participation Method:
1. Upload your dance cover video titled 'Crayon Pop FM cover' on Youtube.
2. Send your Youtube link and participants' name to Crome Entertainment admin email account (admin@chrome-ent.co.kr)
- Participation Period: March 25th, 2015 - April 11th, 2015 (KST)
- Winner Announcement: April 18th, 2015 (through Crayon Pop Official Twitter)
- Method Selecting Winner: Crayon Pop will choose 10 videos themselves (including clever idea and views)
- Prizes: autographed CD

Original video belongs to Chrome Entertainment and Crayon Pop