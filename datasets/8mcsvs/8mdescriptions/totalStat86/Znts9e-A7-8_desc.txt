Programa exibido dia 21/09/09 com Douglas Las Casas - bateria

"Criando ao Vivo com Las Casas" 
A idia do projeto  montar um grupo diferente a cada programa, incentivando e valorizando compositores, arranjadores e msicos, fazendo um ensaio aberto, onde um dos msicos chega com a msica composta escrita em partitura, sem que os outros msicos saibam do que se trata, distribui as partituras a todos e comeamos a fazer os arranjos de: temas, improvisos, grooves, melodias, tudo com as cmeras ligadas. Isso  muito interessante, pois sempre que ouvimos uma msica, ela j nos chega pronta aos ouvidos, e os leigos, estudantes e interessados em msica, no tem a menor idia de como aquela msica que est ouvindo foi projetada, arranjada, discutida e montada. E  exatamente isso que queremos mostrar com esse programa, como  feita toda a construo de uma msica que normalmente  produzida nos estdios e depois gravada. Mas agora com acesso a todos e cmeras ligadas. 
O programa  ao vivo e transmitido pela internet, no Auditrio Cia da Msica, e vai ao ar todas as segundas feiras das 17:30 s 19:00 hrs pelo site: www.tvciadamusica.com.br 
Entre, participe e fique avontis!! 
Las Casas