The second chapter of Episode 6 of the Lego Star Wars Series. [1/2]
Chapter Overview:
Well, everybody who went to Jabba's Palace got caught. Which is... everybody. But, Luke, being the scheming little guy he is, devises a plan that involves Lando in disguise and R2D2 helping them escape. So, once they are free, they have to blow up the giant ship and fly away on a skiff.