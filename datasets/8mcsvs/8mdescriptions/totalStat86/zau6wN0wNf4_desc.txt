Good news! My only guppy had given birth yesterday! 

I'm so excited but worry about the fry being eaten by their mother that I searched online for a breeding box.

I found some interesting designs but most of the better designs are the hanging external breeding/holding box with air lift bubble to provide fresh water & overflow out back into the bigger tank.

However, when I rushed to the fish shop early in the morning 6am to find that that particular fish shop don't have such breeder tank.

I then found a External Cycle Betta Tank for fighting fishes that uses similar "air lift" concept to push water around in the tank.

So I bought that at $13 which the shop keeper reduced down to $12 hehe.

I went home and modified it to become the internal bubble air lift cycle breeding box with filter!

First I put the air lift tube outside of the tank and secured using two rubble band.

I use soldering iron to make 6 holes on the left side of the tank where there is filter medium so that the fry don't fall out of the tank with the water.

I covered the air lift tube with a filter bag to prevent sucking shrimps into the breeding tank.

And the internal breeding tank is completed!

I'm not sure if there is any other product out there that does similar.

There is definitely an advantage to have the breeding tank inside the fish tank namely having similar water temperature and not sticking out of the fish tank.

Well, enjoy!

Credits
External Hanging Breeder box image
http://tinypic.com/view.php?pic=2exrk1c&s=5#.U7J6xxaUfGs

Background Music from Youtube Audio Library 
Title: How_About_It.mp3