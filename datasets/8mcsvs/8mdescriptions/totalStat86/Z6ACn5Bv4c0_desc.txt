S&M contains performances of Metallica songs with additional symphonic accompaniment, which was composed by Michael Kamen, who also conducted the orchestra during the concert. The idea to combine heavy metal with an epic classical approach, as James Hetfield stated repeatedly, was an idea of Cliff Burton. Burtons love of classical music, especially of Johann Sebastian Bach, can be traced back to many instrumental parts and melodic characteristics in Metallicas songwriting including classics from Ride The Lightning and Master of Puppets.

In addition to songs from previous albums spanning Ride the Lightning through ReLoad, there are two new compositions: "No Leaf Clover" and "Human", as well as a symphonic cover of "The Ecstasy of Gold" by Ennio Morricone. "No Leaf Clover" has since been performed by Metallica in concert, using a recording of the orchestral prelude.

Several other songs, including "Wasting My Hate", "The Unforgiven", and "Fade To Black", were considered for selection, but were eventually dropped as it was decided by both Metallica and Michael Kamen that they weren't well suited for symphonic accompaniment.

Changes were made to the lyrics of some songs, most notably the removal of the second verse and chorus of "The Thing That Should Not Be".

The "S" in the stylized "S&M" on the album cover is a backwards treble clef, while the "M" is taken from Metallica's iconic logo. "S&M", which ostensibly stands for "Symphony & Metallica" is a play on words referring to Sadomasochism, which is often abbreviated as "S&M".