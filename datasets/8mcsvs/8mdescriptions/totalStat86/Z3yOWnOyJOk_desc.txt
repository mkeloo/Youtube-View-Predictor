http://www.Zilchebooks.wordpress.com

Got a new Kindle? You'll first want to learn how to connect to a wireless network so that you'll be able to search for and download ebooks and other content from the Kindle Store, right from your Kindle. 

If your Kindle includes Free 3G, wireless connectivity is automatic. If you see a 3G network indicator (3G, EDGE, or GPRS) in the upper right corner of your Kindle screen, your Kindle is already connected wirelessly.

http://amzn.to/coROkj


Kindle 3
Kindle DX
Amazon
Amazon.com
Connect
Wirelessly
Wi-Fi
3G
How to