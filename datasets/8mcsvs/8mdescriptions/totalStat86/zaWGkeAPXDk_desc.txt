DESPICABLE ME: MINION RUSH - ALL LEVELS WALKTHROUGH in PLAYLIST - https://www.youtube.com/playlist?list=PL8wZKON07iXVPjHcw8ziLCYxDUcsqX2_m

Despicable Me: Minion Rush (commonly known as Minion Rush) is a mobile action video game inspired by the Despicable Me franchise.

Players play as Dave (The default character in the game) or other Minions and compete with others in hilarious, fast-paced challenges. Players can do various tasks like defeating villains to earn the title "Minion of the Year". The game also allows customization of the Minions.

Costumes:
In Minion Rush, players can customize, buy and unlock different costumes.

Purchasable:
Astronaut Minion (limited time only), Baby Minion, Baker Minion, Ballerina Minion, Bee-do Minion,, Bogatyr Minion (Russian only), Boxer Minion (limited time only), Cupid Minion (limited time only), Dad Minion, Dancer Minion (limited time only), Disguised Minion (puzzle pieces only), Firefighter, Minion, Frankenstein Minion (limited time only), Girl Minion, Golfer Minion, Grandpa Minion (puzzle pieces only), Hunter Minion, Jelly Jar Minion (limited time only), Jogger Minion, Knight Minion (limited time only), Lifeguard Minion (limited time only), Lucy Minion (limited time only), Magician Minion (limited time only), Maid Minion (limited time only), Mariachi Minion (limited time only), Mom Minion, Ninja Minion, Party Minion (limited time only), Quarterback Minion, Referee Minion, Santa Minion (limited time only), Singer Minion, Snorkeler Minion, Snowboarder Minion, Starfish Minion, Surfer Minion, Tortilla Chip Hat Minion (puzzle piece only), Tourist Minion, Vacationer Minion, Vampire Minion, Worker Minion

Unlockable:
Kung Fu Taunt, Laughing Taunt, Referee Minion (Past), Baby Minion (Present)

Bosses:
Vector, Meena, El Macho, The Villaintriloquist

Locations:
Gru's Lab, Holiday Lab (limited time only), Grus Residential Area, Halloween Residential Area (limited time only), El Macho's Lair, Minion Beach, Paradise Mall, Downtown, Anti-Villain League, Super Silly Fun Land, Jelly Lab, The Volcano, Vectors Fortress, Halloween Vector's Fortress (limited time only), The Arctic (limited time only), Minion Park

Power-ups:
Golden Banana, Golden Shield, Freeze Ray, Golden Prize Pod, Banana Vacuum, Banana Splitter, Minion Shield, Minion Launcher, Grus Rocket, Fluffy Unicorn, Mega Minion, Moon, PX-41, Snowboards (limited time only), Skateboards (limited time only)

Currency: Banana, Token, Puzzle Piece, Other Items, Bapples '(No longer exist in Jelly Lab Update), Cake Slices, Bow Ties, Roses

Perks: Jump x2 (each jump is counted twice), Slide x2 (each roll is counted twice), Despicable Action x2 (each Despicable Action is counted twice), Pickups x2 (each item picked up is counted twice), Banana x2 (each Banana picked up is counted twice), Slides x2 (each use of a slide is counted twice) 

My Channel - http://www.youtube.com/user/iGameplay1337
Subscribe - http://youtube.com/user/iGameplay1337?sub_confirmation=1
Follow my Twitter - http://twitter.com/Andromalic
My Apple GameCenter ID: Andromalic (USA)
PSN ID: iGameplay_1337 (USA)
NEW STEAM ACCOUNT: ANDROMALIC
ADD TO FRIENDS !