Deze video toont u beeld voor beeld hoe het bronzen kunstwerk "Jagende cheeta" ontstaat. U ziet hoe Carla Vrendenberg de cheeta boetseert en u bent erbij als de mal en het holle wasmodel gemaakt worden. U staat vooraan in de gieterij bij het gieten van de kunstwerk en bij het afwerken en patineren.

Deel 1 van 2 

Sites:
http://www.carlavrendenberg.nl
http://www.mijmertuin.nl

Extra info:
http://www.uitinhetzuiden.nl/htm/Brabant_pages/deelnemers/kunst_design/CarlaVrendenberg_Drunen.html
http://www.boekscout.nl/html/boek.asp?id=1323