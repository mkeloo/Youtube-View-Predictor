Grand Hotel VILLA SERBELLONI, Bellagio, Como lake, Italy

The Grand Hotel Villa Serbelloni is one of the oldest and most elegant hotels in the Lake Como area and the only 5 star de-luxe hotel in Bellagio. 
From April to November the salons and the large lakeside garden offer guests the chance to enjoy a holiday far from the madding crowd and close to nature immersed in one of the most beautiful panoramas in the world.

Il Grand Hotel Villa Serbelloni  uno degli alberghi pi antichi ed eleganti di tutta la zona del lago di Como ed  l'unico 5 stelle lusso i Bellagio. Da aprile a novembre i suoi salotti e il grande giardino in riva al lago offrono agli ospiti la possibilit di vivere una vacanza lontano dalla folla, vicino alla natura e immersi in uno dei panorami pi belli del mondo.

www.villaserbelloni.com