"Like" on facebook: http://facebook.com/heavyglowband

All Heavy Glow songs available on I-Tunes: 

http://itunes.apple.com/ie/artist/heavy-glow/id307427836
http://heavyglowmusic.com

"It's Too Late" the first single from Heavy Glow from their self-titled 2009 release. Filmed in 2012 specifically for airplay on "Arbor Live" in Canada Season 3. "It's Too Late" produced by Stevie Salas for South Apache, recorded at Drac Studios (Matt Sorum of Velvet Revolver, Guns-n-Roses, The Cult), and mixed by Alex Todorov. Features Jared Mullins on guitars and vocals, Joe Brooks on bass, and Dave Rollans on drums. Special guests include Katelin James and Princess Villato Lorenz. Video directed by Thomas Dadourian and the Triton Television staff UCSD. Copyright Heavy Glow 2009. Trailing St. Judas Music (BMI.)

Lyrics: 

She is the air
One stare and all your money
Will soon be gone. 
She's not just anybody. 

Now I bet that you'll play hard to get
It's too late in the day for running. 

It's like an art-false start-
The words are running.
I'd tell a joke 
But she's no anybody. 

Now I bet that you'll play hard to get 
It's too late in the day for running.