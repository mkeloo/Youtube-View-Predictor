HOTEL EN VENTA
NUEVA CONSTRUCCIN EN CENTRO DE BUENOS AIRES
(A 200 METROS DEL OBELISCO)

POR INFORMACIN
TELFONOS: (0039) 338-9334195 PROPIETARIO
                      (0054) 15-32321261 PROPIETARIO


HOTEL CERRITO 180 - DATOS TECNICOS DEL INMUEBLE


El Hotel se encuentra ubicado en la calle Cerrito 180 esquina Juan Domingo Pern, a 200 metros del Obelisco, sobre la avenida 9 de julio.

Cuenta con 15000 metros cuadrados de superficie cubierta y 800 metros cuadrados de balcones.  

Tiene 36 metros de frente sobre la calle Cerrito (Av. 9 de julio); y 35 metros sobre la calle Pern. Todo el frente del edificio esta completamente revestido en mrmol Egipcio de 2 centmetros de espesor.     

POSEE 4 SUBSUELOS

En el 1 subsuelo tiene una superficie de 1047 metros cuadrados aproximadamente y cuenta con instalaciones propias del Hotel como por ejemplo:
transformador elctrico de alta tensin y sus tableros de medicin; generador elctrico diesel de emergencia; 4 depsitos para usos varios; sala de calderas para agua caliente con sus respectivas bombas; vestuarios y 30 cocheras para el personal. 

En el 2 y 3 subsuelo se encuentran los tanques de agua y lugar para 90 cocheras.

El 4 subsuelo es de uso tcnico ya que all se encuentra alojada la sala de 
maquinas de incendio con todas las bombas necesarias para el sistema.
 
PLANTA BAJA Y PRIMER PISO

En  Planta Baja, el Hotel posee una imponente entrada compuesta por 4 puertas de bronce pulido,  amplios ventanales y una envolvente escalera totalmente revestida en mrmol Botticino con barandas de bronce.
Los mostradores de la recepcin y el Bar estn revestidos en mrmol Rosso Asiago.
3 modernos ascensores  principales marca THYSSEN KRUPP (Alemania) con capacidad para 10 personas cada uno, con puertas en bronce y un cuarto ascensor de servicio para 15 personas.

Tanto Planta Baja como 1 piso cuentan con la totalidad de sus pisos y paredes revestidos con mrmol Botticino, y todas sus columnas con mrmol Rosso Asiago como as tambin 20 macetas de gran tamao hechos en mrmol de la misma calidad.   

El Hotel tiene una entrada lateral independiente para el ingreso del personal; en su parte trasera cuenta con 2 oficinas para la administracin con su respectivo bao y patio interno.
Tambin encontramos la amplia cocina del Hotel parcialmente equipada con equipamiento importado de Italia; oficina y comedor para el personal; y dos depsitos equipados.
Tanto 1 piso como Planta baja cuentan con amplios baos completamente revestidos en mrmol. 
En el 1 piso del Hotel encontramos 2 salones comedores, uno de los cuales esta parcialmente equipado, 2 salas de reunin y un saln multiuso de 150 metros cuadrados para eventos, todos estos independiente uno de otro.

HABITACIONES

Las habitaciones se encuentran del 2 al 12 piso con un total de 193. 

17 suites de 70 metros cuadrados con balcn independiente.

8 mini suites de 40 metros cuadrados.

168 habitaciones comunes de un promedio 35 metros cuadrados. En este punto vale la pena aclarar que el 70% de las mismas son tan amplias que podran ser amuebladas con 2 camas matrimoniales dependiendo las necesidades del Hotel.

Todas ellas, al igual que los pasillos, poseen pisos de parquet en Roble de 2 centmetros de altura importado de Italia.

 Las paredes estn revestidas con papel vinlico proveniente de Italia al igual que sus puertas construidas en MDF ignifugas enchapadas en madera de fresno.

Todas las habitaciones cuentan con ventanas marca SCHUCO importadas de Alemania de gran tamao permitiendo una gran entrada de luz, y un sistema Doble Vidrio Hermtico con cmara de aire para un mayor aislamiento trmico.

Cada habitacin, al igual que todo el Hotel cuenta con aire acondicionado y calefaccin central regulable independientemente, marca TOSHIBA. 

Las habitaciones cuentan con bao privado totalmente revestido en mrmol, en el cual tambin encontramos el plato de ducha, vanitory y bacha realizado en mrmol importado de Italia y equipado con gritera de primera lnea. Los baos tienen 9 metros cuadrados en las habitaciones comunes, y de 12 a 14 metros cuadrados en las suites.


SPA -- PISO 13

Con  Bar totalmente equipado; Hidromasaje tambin revestido en mrmol; Sauna; duchas; consultorios para masajes; y un Solarium de 400 metros cuadrados con piso de mrmol.  

PISO 14

Piscina con espacios verdes, reposeras y macetas de gran tamao con palmeras.   
Todo esto con una privilegiada vista de la avenida 9 de julio.