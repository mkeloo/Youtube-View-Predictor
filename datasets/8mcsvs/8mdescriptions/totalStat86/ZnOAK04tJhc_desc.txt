Westlife's official music video for 'I Lay My Love On You'. Click to listen to Westlife on Spotify: http://smarturl.it/WestlifeSpot?IQid=WlLML

As featured on Coast to Coast. Click to buy the track or album via iTunes: http://smarturl.it/WestlifeCTCiTunes?IQid=WlLML
Google Play: http://smarturl.it/WestlifeLMLOYGplay?IQid=WlLML
Amazon: http://smarturl.it/WestlifeILMLOYamazon?IQid=WlLML
Stream more music from Westlife here: http://smarturl.it/WestlifeMultiStream?IQid=WlLML

More from Westlife
My Love: https://youtu.be/ulOb9gIGGd0
You Raise Me Up: https://youtu.be/9bxc9hbwkkw
If I let You Go: https://youtu.be/7NrQei36fJk

More great 00's videos here: http://smarturl.it/Ultimate00?IQid=WlLML

Follow Westlife
Facebook: https://www.facebook.com/Westlife
Twitter: https://twitter.com/westlifemusic

Subscribe to Westlife on YouTube: http://smarturl.it/WestlifeYTSub?IQid=WlLML

---------

Lyrics:

Just a smile and the rain is gone
Can hardly believe it (yeah)
There's an angel standing next to me
Reaching for my heart

Just a smile and there's no way back 
Can hardly believe it (yeah)
But there's an angel, she's calling me
Reaching for my heart

I know, that I'll be ok now
This time it's real

I lay my love on you
It's all I wanna do
Everytime I breathe I feel brand new
You open up my heart
Show me all your love, and walk right through
As I lay my love on you

I was lost in a lonely place
Could hardly even believe it (yeah)
Holding on to yesterdays
Far, far too long