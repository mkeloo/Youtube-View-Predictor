Skoda unveiled the all-new Superb in Prague. Expected to launch globally in June 2015, the new third-generation Superb is based on VW's MQB platform architecture that it shares with cars like the new Passat, Octavia and Golf. Shapur Kotwal gets up close and personal with the new car.

Autocar India is your one stop source for test drive reviews & comparison test of every new car released in India. We also offer a great mix of other automotive content including podcasts, motor show reports, travelogues and other special features.

Visit http://www.autocarindia.com for the latest news & happenings from the auto world.
Facebook: http://www.facebook.com/autocarindiamag
Twitter: http://www.twitter.com/autocarindiamag
G+: https://plus.google.com/+autocarindia1