A review and tutorial of the Cuisinart Keurig brewing system. This single serve coffee brewer is one step up from the Keurig b70 brewer.Make sure to do a search for Keurig coupons to get the best deals. It's not cheap but worth the cost!

Details:
-Fully Programmable
-Choice of 5 cup sizes (4 oz., 6 oz., 8 oz., 10 oz. and 12 oz.)
-Removable 80 oz. water reservoir
-Quiet Brew Technology
-Energy Savings Mode -- Auto On/Off

Cuisinart SS-700 Single Serve Brewing System, Silver - Powered by Keurig 
http://amzn.to/WjWFqt

bfvsgf