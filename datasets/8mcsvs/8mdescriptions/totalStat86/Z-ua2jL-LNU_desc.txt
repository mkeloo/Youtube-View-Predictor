Livestream: http://www.twitch.tv/XerainGaming
Website: http://www.xeraingaming.com
Twitter: http://www.twitter.com/XerainGaming
Facebook: http://www.facebook.com/xeraingaming
More Videos: http://www.youtube.com/XerainGaming

XerainGaming server IP: 87.73.98.227

Minecraft - "THE HUNGER GAMES" Livestream announcement

This is an announcement video for our livestream event 'The Hunger Games' being held on the XerainGaming server
and livestreamed on Twitch.tv on the 21st April

We're looking for 24 people to play 'The Hunger Games'! If you would like to enter for a place, LIKE, COMMENT and 
FAVOURITE this video, then post on the forum topic found here: http://www.xeraingaming.com/forum/10-news/1526-the-hunger-games-livestream-event-enter-here-, leaving your YouTube username
(so we can check you have completed all the criteria ;D) and Minecraft ingame name.

Posting on the same forum topic also automatically places you in a reserve list on a first come first serve basis!

We will be announcing the winners on Thursday in a YouTube video

The winner of 'The Hunger Games' event will gain a guaranteed role in our Minecraft series Wrecked and VIP status
on the XerainGaming server!

The livestream will be held at 7PM (19:00) GMT UK time

Also if you wish, feel free to share this video on Facebook & Twitter! :P

Map 'The Survival Games' by 'Vareide' available here: 'http://www.planetminecraft.com/project/the-survival-games/'

You can follow our Minecraft adventures and other game videos by subscribing to our channel.

Danny's YouTube - http://www.youtube.com/hellomrdannny
Andre's YouTube - N/A Subscribe to Dannys Solo Gaming Channel: http://www.youtube.com/infernodan