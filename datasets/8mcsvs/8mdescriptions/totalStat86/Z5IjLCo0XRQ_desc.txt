Learn how to clean your home electronics and accessories, like  DVD players, computer keyboards, DV cameras and much more in this free how-to video on cleaning personal electronics.

Expert: Bobby Hester
Bio: Bobby has several years of experience in construction and remodeling. He is very detail oriented and takes pride in making sure all of his work is of a high standard.
Filmmaker: bobby Hester