I like this mission, for an A-to-A mission, the music makes the whole ordeal quite calming. As you know, the music question at the start of this mission dictates what the next two missions are during a campaign run. Don't worry, I've recorded all of them. I know I said 'no' this time, but I'm going to upload each mission with their campaign introduction slides, because they don't appear if you choose a mission in Free Mission mode.

Here's how the next four video uploads will play out:

Upload #1 - 11A: 'Chain Reaction'
Upload #2 - 11B: 'Reprisal'
Upload #3 - 12A: 'Powder Keg'
Upload #4 - 12B: 'Four Horsemen'

The popular belief is that Chopper's broken up question during the jamming equates to: "Hey, Kid, you think Nagase's the prettiest girl in the Air Force?"

She's pretty cute, I'll give. But her constant talks of peace are a turn off, I bet that's all she'd talk about during a dinner date.