http://www.beadaholique.com/swarovski-crystal/swarovski-crystal-beads/3700-margarita-beads - Lovely Swarovski crystal Christmas trees have been built inside dazzling Swarovski crystal pave thread rings to create a pair of earrings that are sure to get noticed this holiday season. This video shows how to make two variations on the design. 

Designer: Julie Bean

Related Project: 

The Magic of Christmas Earrings
Project E793
http://www.beadaholique.com/the-magic-of-christmas-earrings.html

You can find the supplies in this video at Beadaholique.com

Swarovski Crystal, 8500102 2-Hole Thread Pave Bead Frame Ring 18.5mm, 1 Piece, Crystal
SKU: SWCR-9005
http://www.beadaholique.com/swarovski-crystal-8500102-2-hole-thread-pave-bead-frame-ring-18-5mm-1-piece-crystal.html

Swarovski Crystal, 8100102 2-Hole Thread Pave Bead Frame Ring 16.5mm, 1 Piece, Crystal
SKU: SWCR-9004
http://www.beadaholique.com/swarovski-crystal-8100102-2-hole-thread-pave-bead-frame-ring-16-5mm-1-piece-crystal.html

Swarovski Crystal, 3700 Flower Margarita Beads 12mm, 4 Pieces, Emerald
SKU: SWC-9810
http://www.beadaholique.com/swarovski-crystal-3700-flower-margarita-beads-12mm-4-pieces-emerald.html

Swarovski Crystal, 3700 Flower Margarita Beads 12mm, 4 Pieces, Crystal Vitrail Medium UNF
SKU: SWC-9816
http://www.beadaholique.com/swarovski-crystal-3700-flower-margarita-beads-12mm-4-pieces-crystal-vitrail-medium-unf.html

Swarovski Crystal, 3700 Flower Margarita Beads 10mm, 6 Pieces, Emerald
SKU: SWC-8750
http://www.beadaholique.com/swarovski-crystal-3700-flower-margarita-beads-10mm-6-pieces-emerald.html

Swarovski Crystal, 3700 Flower Margarita Beads 10mm, 6 Pieces, Vitrail Medium
SKU: SWC-8704
http://www.beadaholique.com/swarovski-crystal-3700-flower-margarita-beads-10mm-6-pieces-vitrail-medium.html

Swarovski Crystal, 3700 Flower Margarita Beads 8mm, 12 Pieces, Emerald
SKU: SWC-8550
http://www.beadaholique.com/swarovski-crystal-3700-flower-margarita-beads-8mm-12-pieces-emerald.html

Swarovski Crystal, 3700 Flower Margarita Beads 8mm, 12 Pieces, Vitrail Medium
SKU: SWC-8504
http://www.beadaholique.com/swarovski-crystal-3700-flower-margarita-beads-8mm-12-pieces-vitrail-medium.html

Swarovski Crystal, 3700 Flower Margarita Beads 6mm, 12 Pieces, Emerald
SKU: SWC-9750
http://www.beadaholique.com/swarovski-crystal-3700-flower-margarita-beads-6mm-12-pieces-emerald.html

Swarovski Crystal, 5601 Cube Beads 4mm, 10 Pieces, Crystal Golden Shadow
SKU: SWC-2408
http://www.beadaholique.com/swarovski-crystal-5601-cube-beads-4mm-10-pieces-crystal-golden-shadow.html

Swarovski Crystal, 5328 Bicone Beads 3mm, 25 Pieces, Light Siam AB
SKU: SWBB-13053
http://www.beadaholique.com/swarovski-crystal-5328-bicone-beads-3mm-25-pieces-light-siam-ab.html

Toho Round Seed Beads 15/0 25C 'Silver Lined Ruby' 8 Gram Tube
SKU: JSO-0204
http://www.beadaholique.com/toho-round-seed-beads-15-0-25c-silver-lined-ruby-8-gram-tube.html

Beadsmith Wire Looping Pliers - Concave And Round Nose
SKU: XTL-5032
http://www.beadaholique.com/beadsmith-wire-looping-pliers-concave-and-round-nose.html

Xuron Jeweler's Super Fine Pliers Chain Nose Flat Nose
SKU: XTL-5450
http://www.beadaholique.com/xuron-jeweler-s-super-fine-pliers-chain-nose-flat-nose.html

Xuron Sharp Flush Cutter Pliers - Wire/Soft Flex
SKU: XTL-5600
http://www.beadaholique.com/xuron-sharp-flush-cutter-pliers-wire-soft-flex.html