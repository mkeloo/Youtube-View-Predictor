Welcome to AutoMotoTube!!! On my channel you will find short, (2-5min) walkaround videos of Cars and Motorcycles. Most of my coverage is from Auto and Moto shows in North America and Europe, and I visit different shows: Big, like New York International Auto Show, Detroit North American International Auto Show or Paris Mondial de L'Automobile, to small regional, Classic Car, RV and Boat Shows. 
I have thousands of High Definition videos of different types of vehicles, everything from new, classic, vintage, old cars, hot rods, motorbikes, recreational vehicles, motor boats, yachts, to airplanes, bicycles, and even tractors :-)
In most of my videos, I take a look at the exterior design and interior arrangements of the vehicle, so you can receive a general idea and appreciation of a certain brand or model.

I really appreciate all your comments and critics - they help a lot, in building one of the most diversified Auto & Vehicles Channel on YouTube!!!

P.S: To find a video of a certain model in my channel, just write the brand and model name in the search bar above, or have a look in my playlists.

You can find me on my official facebook and twitter pages:
http://www.facebook.com/automototube 
http://www.twitter.com/automototube

, or if you have chance, check out my web site:
http://www.automototube.net - there I have my videos organized and all the vehicles easy to find. 

Subscription link for my Channel: http://www.youtube.com/subscription_center?add_user=automototube

Thanks for watching and stay tuned!!! A lot more to come...