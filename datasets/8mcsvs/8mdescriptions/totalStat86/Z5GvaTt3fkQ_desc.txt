http://www.export-manga.com

The Casio EX-Word XD-GF9800 contains the best Japanese-English dictionaries as well as many special features.

The ultimate tool for any student of Japanese: whatever your level is, this electronic dictionary will be a great support, without even knowing kanji (ideograms) thanks to its tactile recognition of kanji you know the meaning and pronunciation.

Special Features of this electronic dictionary

 Double-touch panel for easy operation and search.

 A stylus that can be used to write, select and scroll through the menus.

 Writing with stylus to write kanji, kana and Romaji (roman characters) and select the main entrances on both screens and move.

 With voice in Japanese: you can hear 10,000 words spoken in native Japanese.

 Cards with custom features that allow you to make your own cards to review and study.

 A new motion sensor that adjusts the text for easy reading, no matter how you turn the dictionary.

Features of Casio EX-word XD-GF9800

- 50 MB internal memory.

- New screen of 480  320 pixel LCD screen, 5 inches.

- Write input methods: Kana, Romaji, Korean and Russian

- Quick jump between the different dictionaries, different functions and immediate translation search.

- Handwriting recognition of Kanji.

- Encyclopedias with pictures.

- Dictionaries of proverbs, slang computing, business and economics, katakana and kanji.

- Retro lighting.

- SD card slot.

- USB port.

- Speaker and headphone output.

- Battery power: approximately 100-130 hours.

- Size: 110 x 154 mm. width 15.5 mm. height (for the thinnest section), 19.7 mm in height (for the thickest section)

- Weight: 320 g (battery included)

Accessories

 One stylus

 Headphones (3.5 mm connector)

 USB Cable

 Batteries: 2 AAA batteries

Includes basic guide to management of the dictionary in English.