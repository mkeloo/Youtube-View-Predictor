Watch how renowned live performer Dub FX created his unique performance with the TRAKTOR KONTROL S4 all-in-one DJ performance system using TRAKTOR 2 Technology Inside.

He records his voice using a microphone as the live input source in TRAKTOR's Loop Recorder, overdubbing layer after layer to create an intricate vocal arrangement.

Using the filter knob on the S4, he brings in a drum loop on a Sample Deck and adds more layers to the performance. Note how he moves recorded material from the Loop Recorder to empty Sample Deck slots to free the Loop Recorder for further use.

For more info on TRAKTOR KONTROL S4, visit http://www.native-instruments.com/s4

Also, for more detailed product information on the next generation of TRAKTOR, please see: http://www.native-instruments.com/traktor