These are some test shoots where I tried the BMPC 4K Raw in 30p slowed down to 24 and the Vision Color Raw to BMDFilm Log converter. I also blew highlights in every shot just to see how easy they where to recover.
This was also my first run with the FilmCity rig on my tripod. And lastly I used my two Tamrons together and have not corrected them to each other.
If you plan on buying a Blackmagic Camera or the lenses, you can support the channel by using the following link.
BMPC 4K
http://www.bhphotovideo.com/c/product/964119-REG/blackmagic_design_blackmagic_production_camera_4k.html/BI/20174/KBID/14247
Tamron 17-50/2.8
http://www.bhphotovideo.com/c/product/652136-USA/Tamron_AFB005C700_SP_AF_17_50mm_f_2_8.html/BI/20174/KBID/14247
Tamron 70-200mm f2.8
http://www.bhphotovideo.com/c/product/539396-REG/Tamron_AF001C_700_70_200mm_f_2_8_Di_LD.html/BI/20174/KBID/14247


Results:
The 30p slowdown works but since I live in PAL-Land I need to adjust my shutter speed, there are flickering LEDs all over.
My Tripod will due but could hav been a tad more stable. 
The Vision Color BMDFilm is great. They will release a LUT so that the output becomes identical to the BMPC Prores. But since this was all Raw I could  just treat it as 2.5K Raw and didnt need to correct.
The highlight recovery is awesome. Next test will be do overexpose actual subjects.

Facebook: 
https://www.facebook.com/mattiasburlingYT
Please checkout my other videos as well:
https://www.youtube.com/user/mattiasburling

Music:  Silent Partner Court and Page"