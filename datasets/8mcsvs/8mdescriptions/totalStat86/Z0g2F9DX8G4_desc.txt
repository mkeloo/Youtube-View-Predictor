PLEASE READ THE DESCRIPTION BELOW:

Hey everyone! Check out our latest sports trailer featuring Stephen Curry of the Golden State Warriors and some of his 2013 playoffs round 1 highlights against the Denver Nuggets. Don't forget to watch in HD, and we hope you enjoy!

Feel free to leave a (civil) comment below, like, and subscribe!

Music: New Sheriff in Town by X-Ray Dog

Disclaimer: All rights to the content presented - Stephen Curry round 1 playoff highlights - belong to their respective owners, including the National Basketball Association (NBA), NBA TV, ESPN, ESPN2, the Golden State Warriors organization, Stephen Curry, and X-Ray Dog.
This video was made solely for entertainment purposes; no copyright infringement intended.