Title: Counter-Strike: Condition Zero
Genre: Action
Developer: Valve
Publisher: Valve
Release Date: Mar 1, 2004
Languages: English, French, German, Italian, Korean, Spanish, Simplified Chinese, Traditional Chinese


About the Game:

With its extensive Tour of Duty campaign, a near-limitless number of skirmish modes, updates and new content for Counter-Strike's award-winning multiplayer game play, plus over 12 bonus single player missions, Counter-Strike: Condition Zero is a tremendous offering of single and multiplayer content.


System Requirements:

Minimum: 500 mhz processor, 96mb ram, 16mb video card, Windows 2000/XP, Mouse, Keyboard, Internet Connection


Recommended: 800 mhz processor, 128mb ram, 32mb  video card, Windows 2000/XP, Mouse, Keyboard, Internet Connection