The Pride retired number 10 in a pre-game ceremony on Saturday, January 31, 2009.  Speedy Claxton, now with the Atlanta Hawks, played for Hofstra from 1996 to 2000 and scored 2,015 points.  He is the program's all-time leader in assists (660) and steals (288).

Source: http://hofstra.edu/Athletics/MBasketball/ath_mbb_gameresult.cfm?gameID=22BCE163-65B3-F1F2-6DA679D5B2AFA9C1