Recordium review: http://www.iphoneappsfinder.com/utility/recordium-for-iphone/

Recordium is a quality voice recorder for iPhone. It lets you highlight the track while playing or recording audio. You can tag or add notes to your recordings. It is Dropbox, Google Drive, and Evernote friendly.

Download: Recordium - Highlight & Annotate Voice Recordings