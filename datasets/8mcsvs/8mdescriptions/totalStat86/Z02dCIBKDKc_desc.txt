Adventure Kids TV is here for it's very first Surprise egg toy opening! Super cute and outgoing three-year-old Kennadee opens up 9 surprise eggs to find all sorts of amazing kids toys.  Kennadee enjoys watching surprise eggs and blind bag videos and wanted to created her very own - this was her very first video.  These surprise eggs include toys from Peppa Pig, Daniel Tiger, Dora The Explorer, Thomas the Train and more.  Kennadee's favorite toys where the Daniel Tiger's neighborhood and Disney toys.  She loves to watch Daniel Tiger's neighborhood on TV and has lots of Daniel Tiger toys.  Let us know if you like surprise eggs and blind bag videos in the comments.

Kennadee has lots of plans to open more surprise eggs and blind bags real soon, include the chocolate kinder surprise eggs.  Her favorite toy of all in this video are the Daniel Tiger surprise eggs toys - she loves watching Daniel Tiger on TV and YouTube.

The first series inspired by Mister Rogers Neighborhood, Daniel Tiger's Neighborhood features 4-year-old Daniel Tiger, son of the original program's Daniel Striped Tiger. Every day Daniel puts on his red sweater, ties his shoes, and invites a new generation of preschoolers into the Neighborhood of Make-Believe. Daniel shares his daily adventures with pals O the Owl, Katerina Kittycat, Prince Wednesday and Miss Elaina. With the help of Daniel and his friends, preschoolers have fun and learn practical skills necessary for growing and developing.

"Peppa Pig" follows the adventures of the titular, anthropomorphic animal along with her family and friends. Each of her friends is a different kind of animal -- with a last name matching the type of animal each is. 

Click here to Subscribe: http://www.youtube.com/subscription_center?add_user=jamesfabin2

Twitter: https://twitter.com/AdventureKidsTV
Facebook: https://www.facebook.com/Adventure-Kids-TV-914845418633916/
Google+: https://plus.google.com/u/0/+AdventureKidsTV1

New videos every week! Please THUMBS UP, SHARE, and SUBSCRIBE. 

--- MORE ADVENTURE KIDS TV Videos ---
Surprise Eggs and Blind Bags: https://www.youtube.com/watch?v=0FkFSV4rrTQ&list=PLModU0FxBdioZIJA3kfH7jrf3121duwpx

Outdoor Adventures: https://www.youtube.com/watch?v=nlSTG81MYGo&list=PLModU0FxBdipeYVjWeQ1Z2czQbSZodrNT

Taste Test Challenges: https://www.youtube.com/watch?v=VBXych4RDk4&list=PLModU0FxBdiq8wKdnfj8cOG4hhX1kVqkj

Toy and App Reviews: https://www.youtube.com/watch?v=Xc-p2OXiCj4&list=PLModU0FxBdipbDecHj56Ha9zmADwyyfiE

I Spy Videos: https://www.youtube.com/watch?v=POhDOlHzKqw&list=PLModU0FxBdipSCmKCyr5iQU72s8LL8IVV

All Adventure Kids TV Videos: https://www.youtube.com/playlist?list=PLModU0FxBdiqRV3VTewrSXFaZZ3vnbdG-

--- ABOUT Adventure Kids TV ---
Welcome to Adventure Kids TV!  Take a look around Adventure Kids TV channel and you'll find lots of fun, family friendly videos for kids of all ages.  Our videos are designed to help kids explore, teach them imaginative play, learn social skills, play games, review fun kids toys, and overall good family fun.  This was started by our daughter Kennadee who loved watching other kids on YouTube like Hobby Kids, Ryans Toys Review, Doh Much Fun, Evan Tube HD and many others (make sure and check out all of those channels also for great kid videos).  It didn't take long for her younger brother to join in.  Adventure Kids TV produces safe family friendly kid shows that you can trust - with a new Adventure Kids TV video posted every week.  Today we do toy reviews, visit fun places, open surprise eggs and blind bags, play I Spy, review apps, and we even eat some pretty crazy stuff in our taste test challenges.  Our family is made up of Adventure Kennadee, Adventure Cameron, Adventure Dad, Adventure Mom, and Adventure Dogs Shiloh and Amsterdam. We love sharing fun educational learning and our familys fun adventures.  Show your support by subscribing to the Adventure Kids TV channel, giving our videos a thumbs up and writing comments.