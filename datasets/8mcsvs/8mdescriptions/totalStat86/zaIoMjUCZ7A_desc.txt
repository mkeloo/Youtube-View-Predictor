October 24, 2012
I really like using this semi-clear shelf liner on my upper cabinet doors. It blurs enough to "hide" what's in the cabinets but clear enough to find things without hunting.