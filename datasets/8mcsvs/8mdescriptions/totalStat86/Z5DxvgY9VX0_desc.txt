Kolang-kaling (buah atap) adalah nama cemilan kenyal berbentuk lonjong dan berwarna putih transparan dan mempunyai rasa yang menyegarkan. Kolang kaling yang dalam bahasa Belanda biasa disebut glibbertjes ini, dibuat dari biji pohon aren (Arenga pinnata) yang berbentuk pipih dan bergetah. Kandungan karbohidrat yang dimiliki kolang kaling bisa memberikan rasa kenyang bagi orang yang mengonsumsinya, selain itu juga menghentikan nafsu makan dan mengakibatkan konsumsi makanan jadi menurun, sehingga cocok dikonsumsi sebagai makanan diet. -wikipedia.com- Selain itu Kolang kaling juga memiliki beragam khasiat lainnya yaitu, memperkuat tulang, memperlancar pencernaan hingga mengobati radang sendi.

Resep lengkap bagaimana cara membuat Es Kolang Kaling  dapat dilihat di bawah.

Bahan:
kolang kaling :
250 gr kolang kaling
100 gr gula
500 ml air
3 sdm air jeruk nipis

pelengkap :
100 gram daging kelapa muda yang sudah dikerok
200 ml rebusan santan kental
200 ml air gula
susu kental manis secukupnya
es batu secukupnya atau es serut secukupnya


Temukan halaman g+ kami di http://google.com/+masaktv
Ikuti twitter kami di https://twitter.com/MasakTV
Likes facebook kami https://www.facebook.com/masakdottv

Jangan lupa kami tunggu foto hasil buatan anda pada akun twitter kami @MasakTV.

Selamat mencoba.