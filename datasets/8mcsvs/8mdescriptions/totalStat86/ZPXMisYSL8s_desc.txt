Sambal Dadak Khas Sunda

Sambal dadak cukup terkenal di Indonesia khususnya di tanah Sunda. Biasanya rumah makan Sunda selalu menyediakan resep sambal dadak ini sebagai pendamping aneka masakan Sunda terutama olahan makanan yang digoreng. Sambal dadak ini disajikan didalam cobek, karena baru akan di ulek jika ada yang memesan oleh karena itu dinamakan sambal dadak (dadakan).

Bahan: cabe merah keriting, cabe rawit, asam, garam, gula pasir, terasi

Cara membuat

 1. Haluskan cabai merah keriting, cabai rawit, terasi asam, gula pasir, dan garam. Tambahkan air jeruk nipis/limau, aduk rata.
    
2. Tempatkan sambal dadak di piring saji. Hidangkan sebagai pelengkap lalapan.