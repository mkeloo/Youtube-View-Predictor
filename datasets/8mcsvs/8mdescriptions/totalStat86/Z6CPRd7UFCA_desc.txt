Optimum 9200 "Almond Milk" on Getting into Raw cooking with Zane

Almond Milk

1 cup Almonds

1 Vanilla seed/pod

1 Litre water

Add the ingredients to optimum 9200. Blend, blend, blend!


Remember to subscribe to Froothie's YouTube channel (FroothieTV), there are many recipes to come! :)

Pick up your very own OPTIMUM blender at http://www.froothie.com.au, http://www.froothieblenders.co.uk or http://www.froothie.co.nz

And for daily recipes, health & wellness tips LIKE: http://www.facebook.com/froothie

Love Zane xx
Raw Food Guru