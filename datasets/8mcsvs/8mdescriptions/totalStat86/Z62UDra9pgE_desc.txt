Pangaea VIP Party Friday Night City of Dreams Manila.  VIP tables for PHP 40,000 ($US 900.00) consumable.  Britney from Miami was our party host for the evening she loves Manila and is on contract for 1 year.  Renowned for pioneering the Ultra-Lounge concept in the 90s, Pangaea has become a staple for those familiar with its intimate and sophisticated club set. Its bespoke designed furniture and custom-made dance tiers make this a club where every inch of the space is literally meant to be danced on. Pangaea has hosted A-listers including Madonna, David Beckham, Steven Tyler, Cee-Lo Green, Jessica Alba, Leonardo DiCaprio, Kate Moss, Will Smith, Jack Nicholson, powerful political figures and global billionaires. The founder of The Ault Group, Michael Van Cleef Ault, is entering the Philippine market for the first time, expanding his nightclub operations in Asia following his most recent international successes in Singapore and India.     

Pangaea Ultra Lounge 
City of Dreams Manila
Open Daily: 10PM Onwards
Tel: +63 917 381 3398

MABUHAY and Welcome to HourPhilippines TV!

Thanks for dropping by.  Please subscribe, like, share and comment on our videos!  We upload daily and weekly!

Your essential guide to unforgettable food, travel and nightlife adventures in the Philippines and beyond!   

Have a great day and a fantastic life ahead!  :-) Cheers,  Lord and Aksana

Food | Travel | Lifestyle | Parties | Nightlife | Events 

For business inquiries, please send us a message :-)

www.HourPhilippines.com