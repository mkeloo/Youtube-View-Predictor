My Little Pony: Friendship is Magic -  Generosity

Season 4 - Episode 8: Rarity Takes Manehattan

And

My Little Pony: Friendship is Magic - Generosity (Reprise) 

Season 4 - Episode 8: Rarity Takes Manehattan

MP3 Download Part 1 Generosity - http://www.mediafire.com/download/1s3zze7v3q032z2

MP3 Download Part 2 Generosity (Reprise)  - http://www.mediafire.com/download/nbba7b49ct23ngd/

Lyrics: Part 1 Generosity

[Rarity]

Oh, Manehattan, what you do to me
Such a huge bustling community
And there's always opportunity
To do the friendly thing

If some are grouchy, pay no mind
Surprise instead with something kind
Lo and behold, you may just find
A smile is what you bring

Bellhop: Welcome to the Mane Fair Hotel! Please allow me to take those bags to your room for you!

Rarity: Only if you'll accept this gratuity first.

Bellhop: Oh-ho-ho! I'll get your change!

Rarity: Do keep it all. I insist!

[Rarity]

Generosity, I'm here to show all that I can give
Generosity, I'm here to set the bar
Just sit back and watch how I live

Rarity: After you.

Tourist Pony 1: Why, thank you.

Rarity: Please, take mine.

Tourist Pony 2: Wow, okay.

[Rarity]

Some may say, "Rarity,
Don't be so big-hearted and bold
Treating strangers like they're friends
This town's too big and cold"

But this is how I play my cards
I'm not about to fold
Where I see a frown, I go to town
Call me the smile patrol

[Rainbow Dash]

Oh, Manehattan, what you do to us

[Fluttershy]

What if you find a Gloomy Gus?

[Applejack]

It's no intimidatin' thing

[Pinkie Pie]

Just be kind without a fuss

[Rarity]

Generosity, I'm here to show all that I can do
Generosity, you are the key
Manehattan, I'm here just for you
Just for you


Lyrics: Part 2 Generosity (Reprise)

Reprise:

[Rarity]

Oh, Manehattan, what have I done?
The thought of Fashion Week was fun
But I went way too far
My friends gave to me in ways so kind
And I gave them nothing but a hard time

And now alone I stand
And now alone I stand

I do not own anything, this is a Hasbro Inc. creation and all copyrights are reserved.

The purpose of the video was just for fun and entertainment and to help people watch their favorite cartoon show.