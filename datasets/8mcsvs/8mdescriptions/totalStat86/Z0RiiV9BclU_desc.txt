Watch mores episode here: https://www.youtube.com/playlist?list=PLkUgLgpUGxYYXi4n8mk78Yq1JySkZdsdR
The moral: Be happy with who you are and what you have.
A fox was frustrated with his short legs, while the elk was dissatisfied with his long, skinny ones. They proposed a swap, and it worked great for a while! But when it came down to the really important thingshunting and gathering foodthe poor friends discovered that they were running into problems. In the end, they decided things were best left the way they were meant to be!
Fables from around the world are brought to life using characters created from different shapes!  This preschool TV series tells retells beloved children's stories from Aesop's Fables to Australian Aborigines folktales using colourful shapes that cleverly forms animals like rabbits, cats and even fish!

Created multi-award-winning animation studio Peach Blossom Media, each episode of this charming series entertains and teaches preschoolers about universal values of love, respect, courage and kindness.


Subscribe to our channel:
https://www.youtube.com/user/PeachBlossomMedia

Our other animation series:

Olive and the Rhyme Rescue Crew.
https://www.youtube.com/playlist?list=PLkUgLgpUGxYZwl58L0iFWS3rVrai3PNSM
Learn the alphabet through sing-along nursery rhymes. Created by the Emmy Award-winning Peach Blossom Media, and headwriter of Mickey Mouse Clubhouse, Leslie Valdes, this unique series teaches the alphabet using your favourite nursery rhymes. Every episode is designed to engage and enrich your young audience, teaching them interesting and useful vocabulary through song and dance.


Watch Humpty Dumpty nursery rhyme here: http://youtu.be/iYMOZpSLhc4


Watch SHAPES in Spanish, Fbulas de Formas: 
https://www.youtube.com/playlist?list=PLkUgLgpUGxYZkH34HCdwpKxIvoGr9_9fm


Watch Taoshu the Warrior Boy here:
https://www.youtube.com/playlist?list=PLkUgLgpUGxYYdyf57ts3lnpdMQjdHU8JM
Winner of the iParenting Award, this is a wholesome series for kids four and above.  It is the story of Taoshu, a bright young boy who lived in ancient China.  Animated in a colourful unique style reminiscent of the traditional paper-cutout folk art of Yunnan, this series is full of gentle humour and teaches kids universal values like kindness, responsibility and love for nature.