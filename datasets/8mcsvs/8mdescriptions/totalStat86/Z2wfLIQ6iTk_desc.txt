"The Avengers" movie hits theaters on May 4, 2012.

The Avengers continues the epic big-screen adventures started in "Iron Man," "The Incredible Hulk," "Iron Man 2," "Thor" and "Captain America: The First Avenger". Starring Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth, Scarlett Johansson, Jeremy Renner and Samuel L. Jackson, and directed by Joss Whedon, "Marvel's The Avengers" is based on the ever-popular Marvel comic book series "The Avengers," first published in 1963 and a comics institution ever since. The Avengers movie trailer 2012, official teaser trailer, is presented in full HD 1080p high resolution.

THE AVENGERS 2012 Movie
Genre: Action and Adventure, Science Fiction
Director: Joss Whedon
Cast: Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth, Scarlett Johansson, Jeremy Renner, Tom Hiddleston, Stellan Skarsgrd , Samuel L. Jackson
Writers: Joss Whedon