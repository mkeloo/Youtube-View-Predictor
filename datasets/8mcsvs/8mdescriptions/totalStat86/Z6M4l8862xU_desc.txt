Official, homegrown video to the track "Eleanor".

Credit, mad props and kittens go to:

Lars Gehrt - editing and post production
Henrik Hjortns - Producer on the original Eleanor track.
Michael Chekov Jensen - main camera man
Thorbjrn Forsberg - secondary camera man and GoPro wizard
Jonas Olsen - for letting us abuse his GoPro

"Eleanor" was written and recorded by Fusskalt in 2013. Copyright, all rights reserved, hellfire and rainbows!