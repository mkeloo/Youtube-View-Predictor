Connect!
Facebook: http://www.facebook.com/alangee15
SoundCloud: http://www.soundcloud.com/alangee15

Download link:
https://soundcloud.com/alangee15/sheiks-theme

I love my mom very much for buying me Ocarina of Time at Target in late winter of '98. It was the first video game I completed with the help of my real shiny strategy guide. As a youngling, I was very proud. 

I was recently showing my 12 year old sister some of the amazing music in the game. I accidentally stumbled on Sheik's Theme and immediately fell in love. It's such a short piece that I don't ever remember hearing in the game. So of course, I thought a slight bounce might sound good on it.

Really hope you guys enjoy this one. Make sure to listen to the original track here:
http://www.youtube.com/watch?v=8Yc7kp_ETGU