J.P. Morgan Premiership Rugby 7s - Final Stage Pool A.

Highlights from Worcester's 35-12 defeat to Leicester Tigers.

The Official YouTube channel of Premiership Rugby with exclusive news, match highlights, player profiles and more.

To find out more about Premiership Rugby visit:
http://www.premiershiprugby.com/home.php
Premiership Rugby on Facebook
http://www.facebook.com/PremiershipRugby
Follow us on Twitter
http://twitter.com/premrugby