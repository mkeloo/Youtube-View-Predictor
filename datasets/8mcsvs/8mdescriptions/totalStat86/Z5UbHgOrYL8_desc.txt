Made with Dinosaur BBQ Muther Sauce!  BBQ Pulled Pork and Baked Mac & Cheese portioned perfectly in a 100 Calorie Wrap!

Like & Follow me @
http://www.facebook.com/hellthyjunkfood/
http://www.twitter.com/ryceq/

Hellthy Pulled Pork
Ingredients:
6 Lbs Pork Shoulder Butt Roast
2 Cups Dinosaur BBQ Muther Sauce
1 Large Sweet Onion
1 Cup Chopped Green Pepper
2 Tbsp Spicy Brown Mustard
2 Tsp Minced Garlic
1 Cup Chicken Broth
1 Cup White Vinegar
2 Tbsp Worcestershire Sauce
1 Tbsp Italian Seasoning

Hellthy Baked Mac & Cheese
Ingredients
4 Cups Uncooked Elbow Macaroni
3 Cups Shredded Sharp Cheddar Cheese
1 Cup Shredded Parmesan Cheese
2.5 Cups Skim Milk
4 Tbsp Light Fake Butter
1/4 Cup All Purpose Flour
10 Ritz Crackers
2 Tsp Spicy Brown Mustard
1 Tsp Frank's Red Hot

Hellthy BBQ Pulled Pork Mac & Cheese Wrap
Ingredients
1 Whole Wheat Wrap
4 oz Hellthy Pulled Pork
1/2 Cup Hellthy Mac & Cheese
2 Tbsp Dinosaur BBQ Muther Sauce

Directions:
Step 1 - Slow Cook Pork Shoulder Butt Roast
Step 2 - Baked Mac & Cheese Time
Step 3 - It's A Wrap Folks
Nutrition Facts