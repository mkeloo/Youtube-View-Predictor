GRAND NARODNA TELEVIZIJA ZA CEO SVET
NET TV Plus https://www.nettvplus.com
Satelit: Eutelsat 16A
Frekvencija: 10972 MHz
Polarizacija: Vertikalna
SR: 27500
FEC: 5/6
SID: 1306

Srbija:
Analogna
SBB -- svi gradovi
KDS
JET TV
Digitalna - Pozicija 211
D3
Beogrid
D3i
D3Go
Total TV - Pozicija 11

Hrvatska:
Total TV

Bosna i Hercegovina:
Telemach
Total Tv - Pozicija 9

Slovenija:
Telemach
Total Tv - Pozicija 104

Crna Gora:
Total Tv - Pozicija 8

Makedonija:
Total Tv - Pozicija 10

Label & Copyright: Grand Production
Zvezde Granda Management : +381 65 88 22 595

Zabranjeno svako kopiranje video i/ili audio snimaka i 
postavljanje na druge kanale!