Baby V.O.X. () - Coincidence/Accident/Oo Yun
(Baby Voices Of  eXpression)

Members:
Kim E-Z () - braids
Lee Hee-Jin () short brown hair
Shim Eun Jin () - headband/red hair
Kan Mi Youn () - blond streaked hair
Yoon Eun Hye () - red hair

Intro Spanish and English lyrics/translation credit: yoroshikumeister

Spanish: 
Baila baila conmigo
Baila baila bonita
Baila baila mueve y la chica baila bonita

English:
Dance, dance with me. 
Dance, dance pretty girl. 
Dance, dance [and] move, the girl dances [so] pretty.
(x2)

The song that plays in the middle is Consent (). You can find the full mv here:
https://www.youtube.com/watch?v=pQAcloWMfIg

Read more about them here:
http://en.wikipedia.org/wiki/Baby_V.O.X.