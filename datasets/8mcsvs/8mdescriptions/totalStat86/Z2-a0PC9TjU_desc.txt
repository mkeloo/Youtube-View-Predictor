Full choreography of "Nitro Bot" by Sentai Express in Just Dance 2014, the successor of Ubisoft's Just Dance 4 game.

Mode: CLASSIC | Duet (2 player) | Difficulty: Medium

HD link for sharing: http://www.youtube.com/watch?v=Z2-a0PC9TjU&hd=1

 Video for gameplay showing purposes only, did not dance during the recording. I like it when there is just the dancer and the pictograms so that I can focus on the choreography and learn it; if you wish to watch videos of me actually playing, please go to my channel page.
 Wii U version footage. My online nickname in JD2014 is BOWSERB.