 : Scriabin, Alexander
http://www.piano.or.jp/enc/composers/34/
2 1 : Sonata for Piano No.2 Mov.1 Andante
Pf.  : Kita Kosuke

 2 Op.19
1 
  http://www.piano.or.jp/enc/pieces/5071/

Pf. [ http://www.piano.or.jp/enc/pianists/detail/316 ]

 Scriabin, AlexanderSonata for Piano No.2 Op.19
Mov.1 Andante 
PTNA Piano Encyclopedia http://www.piano.or.jp/enc/pieces/5071/
About PERFORMERs
Pf. Kita Kosuke[ http://www.piano.or.jp/enc/pianists/detail/316 ]



/*----------++
 PTNA/enc@piano.or.jp
 
 This video is distributed thanks to the recordings offered by the Piano Teachers' National Association (PTNA) and by our collaborators. If you would like to use this video in any public occasion, such as symposium, lecture, thesis, dissertation, and research paper, etc., it is necessary to give the name of performers by citing the 'PTNA Piano Encyclopedia'. For all public use of this video, please inform us of it by email: enc@piano.or.jp (person in charge of Piano Encyclopedia)
 
 Ce film est diffus grce  l'offre volontaire des enregistrements fournis par la Piano Teachers' National Association (PTNA) et ses collaborateurs. Si vous l'utilisez, dans quelque occasion publique, que ce soit pour: une prsentation de colloque, de confrence, un mmoire, une thse ou encore un rapport de recherche, etc., il est ncessaire d'indiquer le nom des interprtes en citant explicitement la PTNA Piano Encyclopedia. Pour toute rutilisation de ce film, veuillez nous en informer par courriel au pralable: enc@piano.or.jp (responsable de PTNA Piano Encyclopedia)
 
 Dieses Video ist dank der Aufnahmen von die Piano Teachers' National Association (PTNA) und unsrer Mitarbeiter verteilt. Wenn Sie dieses Video in jedem ffentlichen Anlass, z. B. Symposium, Vortrag, Dissertation und Forschungsarbeit, usw. benutzen wollen, ist es notwendig, den Namen der Spieler und die 'PTNA Piano Encyclopedia' aufzuschreiben. Fr alle ffentliche Benutzung dieses Videos, informieren Sie es uns per E-Mail: enc@piano.or.jp (Person verantwortlich fr die Piano Encyclopedia)
 
 Questo film  distribuito grazie alle registrazioni fornite dalla Piano Teachers' National Association (PTNA) e dai suoi collaboratori volontari. Se voi volete utilizzarlo, in qualunque occasione pubblica, ad esempi: rappresentazione di convegno, conferenza, o tesi stampata sia accademica che didattica, etc.,  necessario indicare il nome di interprete citando esplicitamente la PTNA Piano Encyclopedia. Per tutte riutilizzazioni di questo film, vi preghiamo di informarcene per email: enc@piano.or.jp (persona incaricata di PTNA Piano Encyclopedia).
 
++----------------------------*/
 PTNA Web Site | 
 http://www.piano.or.jp