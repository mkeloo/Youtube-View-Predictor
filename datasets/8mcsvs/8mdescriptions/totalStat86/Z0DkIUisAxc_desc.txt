Hey guys,
These VLOGS are a tool for you to learn from or to get involved with. Every week im plan to learn a new movement whether its Parkour, Free Running, TRicking, Creative Movement....anything to do with using the human body to move. I invite all of you help me on my journey to increase my level of skills and at the same time yours.

If you have any tips on any of the movements i've done, to help me increase power and make my movement dynamic, please do message me or send me a video response for myself and everyone to learn from. When sending me your video response, please state in the video its for the Mike Wilson Movement VLOGS. My goal is not only to increase my personal level of skills, but to create a big network of people all helping me and others to be the best they can be. Please share my videos as much as you can on your Social Media sites, and help promote the Movement VLOGS.

So heres Movement VLOG No 2 featuring the 'Scoot Kick The Moon Scoot Cork or Corkscrew Combo'. I've been drilling this every day in Ukraine so check the video out to see how i got on.

Please don't forget to subscribe to Sam Parham and Chase Armitage VLOGS and of course the 3RUNTUBE Channel for loads more 3RUN videos on our daily life.

Channels:
Sam Parham - http://www.youtube.com/user/SamParham
Chase Armitage - http://www.youtube.com/user/chase3runmedia
3RUNTUBE - http://www.youtube.com/user/3runtube

Massive thanks for watching. If there are any movements you think i should try learning, whether its parkour, free running or tricking etc, please let me know and i shall mention you in that VLOG.

Please rate, comment and Subscribe........and SHARE :)

Mike Wilson
3RUN Media
http://www.3run.co.uk
http://www.wilsonrunnin.co.uk