Blog Post: http://wp.me/pJgaa-1eZ
Buy from CS Toys: http://tinyurl.com/9lc676o
---
Our fourth Plamonster doesn't belong to Wizard, it doesn't belong to Beast, it's actually the White Wizard's loyal little Plamonster, Black Cerberus. Black Cerberus was first used by the White Wizard to guide Koyomi to Wiseman's lair to retrieve the Water Dragon stone. Unlike the White Wizard's White Garuda being the same as Wizard's Red Garuda, Black Cerberus appears to be exclusive to the White Wizard's arsenal. Included is the Cerberus Wizard Ring. The ring plays "Cerberus! Please!" in the WizarDriver along with the traditional Plamonster summoning noise, followed by a loud barking roar. As with the other Plamonsters, Black Cerberus includes a thick plastic tray that can house the pieces that form the Plamonster. Its action gimmick is a lever on the top of its neck that turns the three dog heads back and forth. It also features two empty ports on its back that can house Garuda's wing pack to make a winged three-headed dog of justice. You can also use the body with other limbs to create what other sort of weird creatures you can think of. As with most of these releases, they aren't absolutely needed for the line, as you can nab the Wizard Ring from a Candy Toy or Capsule Wizard Ring. If you're a fan of the Plamonsters, this is a great addition and one of my favorites. However, if you haven't gotten into the line, this one probably won't change your mind. Recommended for fans of the line, I really like em.
---
Awesome Intro thanks to Ryokuya!
RRR: http://www.ridersrangersandrambles.com
RRR Otoku: http://rrrotoku.com
The RamBoards: http://ridersrangersandrambles.com/forums
My Blog: http://www.shukuenshinobi.com
Follow me on Twitter: http://www.twitter.com/ShukuenShinobi
Like me on Facebook: http://tinyurl.com/4nltrau
Ask me anything: http://www.formspring.me/ShukuenShinobi

Enjoy! Please like, comment, and subscribe!