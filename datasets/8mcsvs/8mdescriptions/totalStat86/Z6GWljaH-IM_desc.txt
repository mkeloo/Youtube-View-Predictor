Sonic CD (JPN/EUR) Soundtrack - Sega Genesis/Megadrive Style
Original MIDI file sequenced by Monster Iestyn.

Album Download links
(ZippyShare)
http://www22.zippyshare.com/v/35017730/file.html
http://www22.zippyshare.com/v/10954665/file.html
(DepositFiles) 
http://depositfiles.com/files/0kxjgyfo9
http://depositfiles.com/files/mpgvi80sg
(4Shared)
http://www.4shared.com/rar/olnfA6R0/sonic_cd_megamix_20.html
http://www.4shared.com/rar/V2qUCmgv/sonic_cd_megamix_20__instrumen.html

-----

You may or may not have noticed that alot of my Sonic CD Genesis remixes have dissapeared from my channel. Well this is what happened. 6 months ago from now, I got a copyright claim on my "Sonic CD - Final Boss Sega Genesis Remix" video from a DeviantArt user who didn't like the fact that I used his/her fan-art for the video. Even though I did give them credit and link to their DA page in the description, this wasn't enough for them and rather than asking me to take the video down, they took the issue up with Youtubes staff and I got a copyright strike on my channel for it! 

As a precaution of this situation not repeating itself again, I have deleted all of my videos that have used Deviant Artwork (which a few exceptions for the people who did reply back to me giving their blessing/consent to use their work). 

Rather than re-upload the album as it was (since I made it a year ago, I have alot of new sounds to my disposal so I decided to yet again remake the album (this is the third time lol) with more VOPM and less soundfonts!