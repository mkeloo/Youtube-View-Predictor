Finely crafted with stainless steel, genuine leather and scratch-resistant glass
Moto 360s fine craftsmanship features a stainless steel case, genuine leather from Horween and scratch-resistant Corning Gorilla Glass. The classic design means it will feel comfortable and familiar on your wrist. 

https://play.google.com/store/devices/details?id=motorola_moto_360_leather_black 

http://www.motorola.com/us/home