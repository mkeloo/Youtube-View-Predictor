The Mini Grundotugger System Trenchless service line replacement with the Mini Grundotugger is simple. Small launch and exit pits are dug at either end of the service line being replaced. A winch cable is rodded from the exit pit, through the existing line, to the launch pit. At the launch pit the Mini Grundotugger's specially designed splitting expander is attached to the winch line. The Mini Grundotugger winch is placed in the exit pit and the winch line is loaded into the unit. The expander forces the split pipe into the surrounding soil. Contractors can choose to pull in the new pipe directly behind the expander or retrieve the split service first, then pull in the new service. 

- Existing pipe replacement
- Split and replace 1/2-inch to 1-inch plastic pipe
- Replace up to 150 feet
- Use 9,000 lb. and 12,000 lb. mini-winch cable pulling system
- Hardened heat-treated steel splitting blades
- Riser splitting and extraction capabilities

Learn more:
http://www.tttechnologies.com/
http://www.tttechnologies.com/products/mini_grundotugger_lateralreplacement.html
http://www.tttechnologies.com/job_stories/