Last night I proposed to the love of my life with this song and video. I hope you enjoy it, her reaction is at the end :)

You can buy the song on iTunes if you like it! :)

http://itunes.apple.com/us/album/my-forever-valentine/id501867647?i=501867711


Song Credits

Written by Dominic DeLauney
Vocals, instrumentals, etc by Dominic DeLauney
Extra guitars by Joe Silkworth
Engineered and Produced by Tony Correlli, At the Deep End Studios  - http://www.thedeependstudio.com 

Video Credits

Flip video by Dominic DeLauney
Cell phone footage of ring shopping by Breanna Shaw
HD awesome footage by Spencer Greer - http://www.spencerscottgreer.com/
Editing by Breanna Shaw - http://www.bybrea.com/ 

 2012 - Dominic DeLauney