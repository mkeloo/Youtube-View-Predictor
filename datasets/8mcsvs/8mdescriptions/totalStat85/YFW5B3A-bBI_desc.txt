*Arranged and mixed by Paul Farrer* 
*uploaded here with permission*

You guys should absolutely go subscribe to Paul! GO! 
Paul's YouTube: http://www.youtube.com/user/livefortodaydiaries
Paul's Facebook: http://www.facebook.com/PaulFarrerMusic
Paul's Twitter: http://www.twitter.com/PaulFarrer1


Hey Geekers!

My buddy Paul asked me if I'd jam with him on the Addams Family theme for his Halloween Special. Paul is a great guitar player with a really good ear for arranging. I'd done a guest solo for his Donkey Kong Country tune a while back and was psyched to see what he'd come up with the Addams Family. Hope you all enjoy it. I'm re-posting this vid here on my channel so that more folks can see it. If you like it, please click over to Paul's channel and subscribe to him! He deserves a big influx of subscribers, lets make it happen!

My next project is working on a couple Christmas collabs that I've got to get ready for my guest soloists so it may be a few weeks til my next cover. Thanks for your patience and all the support! You geekers ...ROCK!

Until next time!
-Tim

=peace=love=rock=CLICKCLICK=

-----------------------------

My MP3's are available here: http://www.loudr.fm/artist/thehumantim/aGx4H

Friend me on FACEBOOK: http://www.facebook.com/thehumantim
Follow me on TWITTER: http://www.twitter.com/thehumantim
Tumbl me on TUMBLR: http://thehumantim.tumblr.com

For GUITAR TABS and even MORE geekery, visit:
http://www.TheHumanTim.com

------------------------------

The Addams Family theme was composed by Vic Mizzy. All credit to him! Paul Farrer arranged this version. Rock on, Paul!