Review van de Diesel Hi-Jack motorhelm.
http://www.motorkledingcenter.nl/diesel-hi-jack.html
De technische kennis van AGV en de onderscheidende creativiteit van Diesel zijn gecombineerd om deze technische helm te creren met aandacht voor elk detail en gelijktijdig in de geest van Diesel. De helikopter piloot gestylde HI-JACK helm is gewijd aan fans van op gepersonaliseerde uitrusting en aan rijders die van het leven op de weg houden, maar tegelijkertijd de onconventionele stijl van Diesel willen uitdragen. De aantrekkelijke look, de verchroomde variaties in de designs en de onmiskenbare vormen aan de zijkant dragen bij aan het luchtvaart design dat de helm nog aantrekkelijker maakt. De helm wordt afgemaakt met camoufleerbare vizieren in verschillende tinten die los bij te bestellen zijn(smoke en spiegel). Zo kan de helm aangepast worden aan de behoeften en stijl van elke rijder en maakt het zijn/haar look nog meer af en eigenzinnig.

Specificaties Diesel Hi-Jack:

- Schaal: ACF glasvezel
- Schalen: 2
- Interieur: Drylex met antibacterile behandeling
- Verwijder- en wasbare voering
- Vizier: anti-kras ISV2 (ook verkrijgbaar in 80% smoke en spiegel vizier
- Vizier mechanisme: vizier vervang- en verwijderbaar zonder gereedschap
- Interne voering in hypoallergene en ademende stof
- Bevestigingssysteem: riem met klik systeem
- Diesel helm