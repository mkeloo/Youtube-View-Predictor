Learn how to make a Love Knot Bracelet or Necklace, using SilverSilk Capture and Findings.

SilverSilk Capture knit received it's name because 6 wires are knitted around  bead chain, CAPTURE-ing the shimmer, light, reflections and strength of the chain. 

With custom designed end caps that have grooves inside to lock to the ball chain you can easily create beautiful jewelry. 

We offer single, double, & triple strand end caps with an attached jumpring. 

Silver Silk can be found at your favorite bead store. To find a bead store near you that offers Silver Silk Capture and End Caps visit www.beadsmith.com