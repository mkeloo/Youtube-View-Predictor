Learn how to play Frederic Chopin's Nocturne "Op 28 No 7" with descriptive teaching and notes.
Feel free to check out my Facebook page: www.facebook.com/elementarypiano
This page will show tutorials posted on YouTube. This page will also be used to take requests and questions about my tutorials.

Gear:
Studio Projects C1 Large Diaphragm Condensor Microphones (2): http://amzn.to/PxLeJj
Creative Labs E-MU Preamp: http://amzn.to/V1Euaw
Monster Microphone Cables: http://amzn.to/V1EF5M
Senheiser HD 280 Pro Headphones: http://amzn.to/O7NETU
Keyboard: I use a really old Roland keyboard that I don't think they even sell anymore. I've heard good things about this keyboard: http://amzn.to/S2ecFg - I recommend trying out whatever keyboard you are thinking about in a store before buying.
Camera: I currently use a Kodak Zi8: http://amzn.to/PxNvV0 - you might also try a later model if you are interested in this camera and price point... maybe something like the Kodak Playsport: http://amzn.to/S2fi3Y
I edit my videos on an Asus U46E-BAL7 computer: http://amzn.to/OyZvKw using Adobe Premier Pro CS6: http://amzn.to/UcwGDx
I edit audio using Reaper: http://www.reaper.fm/