The Charlie Daniels Band is eclectic if nothing else. Of course, this band is a lot else, with its customary tight sound and solid instrumentals. Charlie's vocals will never be mistaken for anything but southern country, but what else do you want? He stands out from the crowd of look-alike sound-alike newcomers with a sound all his own. CDB has become renowned for flag waving in the NRA vein, and the title cut spares no survivors. This song is like one of Abe's split rail fences: you'll be on either side, not riding the middle. CDB offers songs from the eerie to Cajun to rock to country dance: something for everyone. Only one ballad is included, though, and that's a shame. "Guilty" is a song every man should sing to his wife. It's what country music is all about. Most people would be happier with a stronger common theme, but this is vintage CDB. 

Track listing

1  Same Ol' Me  Daniels, Wiseman  3:37
2  Little Joe and Big Bill  Daniels, Jones  3:20
3  Take Me to the Wild Side  Daniels, Jones  5:02
4  My Baby Plays Me Just Like aFiddle  Daniels, Jones  3:09
5  Gone for Real  Anderson, Daniels, Lawler  2:55
6  Sure Beats Pickin' Cotton  Daniels, Jones  3:34
7  Guilty  Blazy, Daniels, Williams  3:56
8  Fais Do Do  Daniels, Jones  3:19
9  Bad Blood  Daniels, Jones  4:33
10  Hit the Ground Runnin'  Daniels, Jones  3:03