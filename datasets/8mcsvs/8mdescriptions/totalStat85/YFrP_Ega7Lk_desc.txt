BONUS: Be sure to see our Disney-exclusive "Tinker Bell Hair Bun" tutorial on the DisneysStyle YT channel: http://youtu.be/M9f2-dEjJ1E

This is not a new hairstyle, and can be seen all over Pinterest, but we wanted to show you how we do it!

I am not a fan of highlights or permanent coloring in my young girls' hair.  What I love about our Chalk Highlights tutorial and this Yarn Extension Braid, is that they are ways to get a cute color accent in the hair without a long-term commitment and potential damage.

Feel free to tag your own photos of this hairstyle with: #CGHYarnFishtailBraid

This fishtail braid with blended yarn strands is perfect for cheerleaders and dancers, but also great for Spirit Week {using school colors} or Crazy Hair Day!  Enjoy!

Items Needed: Brush, rat-tail comb, 1 hair elastic, and 3 or 4 colors of yarn cut into pieces 2x as long as your hair.

Time Requirement: 10 minutes, including adding the yarn extensions.

Skill Level: Medium

To see step-by-step instructions and more photos of this style, please visit...
http://www.cutegirlshairstyles.com

* ~ * ~ * ~ * ~ *

Want to become a "Super Fan" of the week, and potentially have your photo appear in a future video?  Simply email us a photo of you doing something crazy or fun while wearing a CGH t-shirt!  Grab our gear here...

http://bit.ly/VHKvKf

Don't forget to subscribe to our YouTube channel...
http://bit.ly/VovoC9

Feel free to follow us on BlogLovin, where many YT Gurus have beauty blogs...
http://www.bloglovin.com/blog/1815559

Follow CGH on Facebook:
http://www.facebook.com/CuteGirlsHairstyles

Follow CGH on Twitter:
http://www.twitter.com/CuteGirlHair

Follow CGH on Pinterest, and see my "Try" board...
http://www.pinterest.com/MrsHairdo

Follow CGH on Instagram and see unpublished photos of our family:
http://www.instagram.com/CuteGirlsHairstyles


* ~ * ~ * ~ * ~ *
Music: Icing
by Charity Vance (http://www.charityvance.net)
iTunes Link:  http://tinyurl.com/CharityVanceIcing
License: Personal emailed permission from Charity herself.