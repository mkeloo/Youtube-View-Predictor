Results:

15 & Under Division
1st:  Connor Felix
2nd: Jack Collins
3rd: Braeden Adams
4th: Nathan Roy
5th:  Noah Maisonneuve

16-21 Division:
1st:  Sean Marko
2nd: Jake Whitburn
3rd: David Joncas
4th: Brett Krasman
5th: Brett Mills

Women's Open Division:
1st: Mutsumi Ido
2nd: Nicole Oben
3rd: Hitomi Tanaka
4th: Jenaya Jenkins
5th: Elysha Piller

Men's Open Division:
1st: Andy Milenkovic - $250
2nd: Jordan Blain - $150
3rd: Jack Lawrence -$100
4th: Jasper Westbury
5th: Ryoki Ogawa

Rev'd Rider: $100 -- Ryoki Ogawa
Electric's "Gooeyest Move of the Event": $50 And Electric Shades -- Jack Lawrence

Motion Graphics: Michael Wiener

- Music -
Songs: Always Wrong
Artist: Jaill
Release: There No Sky (Oh My My)
Courtesy of Burger Records http://www.burgerrecords.org
iTunes: http://geni.us/3GaU