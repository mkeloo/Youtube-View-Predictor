Watch a short clip from the upcoming episode of Austin & Ally "Couples & Careers" set to premiere on Sunday, May 5th at 8:30pm, a part of Sunday Night of Premieres only on Disney Channel!

Couples & Careers - When Austin & Ally decide to become an "official couple," they agree to separate their professional relationship from their personal relationship. This is much easier said than done when their dating ultimately impacts their song writing.

Check local listings.