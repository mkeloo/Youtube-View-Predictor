Plot: Clint Barton and Bobbi Morse are tasked with taking down an assassin but it turns complicated very quickly when Clint realizes the assassin is his former mentor. His former mentor who betrayed him and tried to kill him. 

Soooo This is what happens when I watch 19 Bond films in 2 weeks but cant actually make a Bond video  Also, I am thoroughly obsessed with Hawkeye, not that you werent already aware of that. 

I also preemptively apologize for that Bond joke, I couldn't resist. 

I think I was about one freeze away from a mental breakdown. The sheer amount of time I spent waiting for Vegas to un-freeze or reload after crashing for the umpteenth time almost drove me mad. Ive had issues before but this was an entirely new level of mechanical failure. 

Clint Barton (Hawkeye)  Jeremy Renner
Bobbi Morse (Mockingbird)  Adrianne Palicki
Buck Chisholm (Trick Shot)  Pierce Brosnan
Young Clint  Isaac Hempstead-Wright

(Canon and Head-Canon ahead) 
Buck Chisholm was a former Navy SEAL. Hes best with a bow (which Im really sorry I couldnt convey in the trailer, it just wasnt working) but is very competent with all ranged weapons. 
He was dishonorably discharged due to his drinking problem and eventually joined the circus as a trick shot performer, gaining his name, Trick Shot. 
He eventually mentors Clint Barton, teaching him to become an archer and a criminal. 
During a bank heist, Clint accidently injures his brother, Barney and has a change of heart. Buck turns on him and shoots him three times and leaves. 
Buck spends the next several years as a hired assassin, gaining a reputation. He gets on S.H.I.E.L.D.s radar and they send Clint, now an agent, to eliminate him, unaware of their past connections. 
Cue trailer plot. 


Music:
Ready or Not  Mischa Book Chillak ft. Esthero
Illumination  X-Ray Dog
{I}m {S}o {S}orr|y  {I}magin|e {D}ragon|s

Clips: The Avengers, The November Man, Thor, Game of Thrones, Mission Impossible: Ghost Protocol, Marvels Agents of S.H.I.E.L.D., The Bourne Legacy, Arrow, S.W.A.T., Die Another Day, The World Is Not Enough, Take, Age of Ultron, Skyfall. 

No copyright infringement intended, all rights to their respective owners.

Other Fan-Made Trailers:
The Darkness
https://www.youtube.com/watch?v=93Ce4SbNPlA
Doctor Strange
https://www.youtube.com/watch?v=-DVqn2BFTNE
Hawkeye: Blindspot
https://www.youtube.com/watch?v=sn-JYUCLXRk
Hawkeye: Vantage Point
https://www.youtube.com/watch?v=c62EIg6VXW0