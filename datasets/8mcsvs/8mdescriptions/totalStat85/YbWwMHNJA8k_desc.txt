NEAERA Prey To Anguish
"Omnicide - Creation Unleashed"


What I have become
What has made myself
I will never turn down
I will never disdain

To the excluded of the wronged
The cursed of the damned
Fear will eat your soul
Without taking a stand

Prey to anguish - Internal dissonance
A state of undeciphered wrath
The sky above
Earth underneath
And hell within

Prisons built from stones of our fears
Worse than giving into failure is giving up hope

Through a downpour of hail and fields of drought
You will rise with the fall

Prisons built from stones of our fears
The sky above
Earth underneath
And hell within