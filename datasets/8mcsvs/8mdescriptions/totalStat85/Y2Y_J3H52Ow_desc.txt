Mostramos como utilizar dois sensores de nvel com flutuador e contato seco para controlar o nvel mximo e mnimo de gua em um reservatrio vertical.

O sensor utilizado  da marca ICOS. Para conhecer mais sobre este produto e baixar os esquemas de ligao e instalao, fluxogramas e aplicaes, visite o site da ICOS:
http://www.icos.com.br/

A montagem que fizemos no vdeo utiliza sensores individuais para controlar o nvel de lquidos, mas ela tambm pode ser feita utilizando sensor de nvel de lquidos de montagem vertical (tipo haste), do qual tambm falamos no vdeo abaixo:

https://www.youtube.com/watch?v=_KoSRyvt738

O controle de nvel de uma caixa d'gua ou outro lquido qualquer (leo, combustvel, gua em alta temperatura, etc..) pode ser feito utilizando sensores de nvel em conjunto com sistemas de controle e acionamento de bombas como um contator com intertravamento ou mesmo um controle digital com Arduino.

Temos um vdeo em que explicamos o funcionamento do contato de selo:
https://www.youtube.com/watch?v=fWblmoYUxJw

E neste outro vdeo mostramos como montar um contato de selo no seu contator:
https://www.youtube.com/watch?v=4J7CtmrAhuY

Algumas das aplicaes da montagem mostrada neste vdeo so:
- controle de nvel em caldeiras
- controle de nvel em caixas d'gua
- controle de nvel de combustvel em veculos
- controle de nvel de gua em um reservatrio de gua de chuva

Como fazer um controle de nvel? Voc ir precisar de 2 sensores de nvel para utilizar no nvel mximo e mnimo. Um contator (pode inclusive ser este da Weg que usamos no vdeo), cabos de conexo para o caso do contator ficar distante dos sensores e alguns canos de PVC para que a montagem dos sensores possa ser feita dentro do reservatrio. Caso prefira furar o reservatrio para inserir os sensores voc no precisar dos canos de PVC.

Neste montagem realizada utilizamos o controle de nvel mximo e mnimo, mas um outro sensor poderia ser conectado para permitir um controle ou sinalizao de nveis intermedirios. 

Se voc tiver qualquer dvida sobre esta montagem ou o funcionamento deste circuito, deixe um comentrio que iremos lhe responder.

--
O Mundo da Eltrica  um canal de eletricidade com vdeos de qualidade para voc aprender pelo celular, tablet ou na TV da sua sala. Eltrica para todos!


Se inscreva no canal Mundo da Eltrica!
http://www.youtube.com/subscription_center?add_user=MundoDaEletrica

Alm deste canal do Youtube, voc pode acompanhar o Mundo da Eltrica nas redes sociais!

Visite o site Mundo da eltrica:
http://www.mundodaeletrica.com.br

Curta o Facebook do Mundo da Eltrica:
https://www.facebook.com/MundoDaEletrica

Siga o Mundo da Eltrica no Google Plus:
https://plus.google.com/+MundodaeletricaBr

Siga o Twitter do Mundo da Eltrica:
https://twitter.com/MundodaEletrica