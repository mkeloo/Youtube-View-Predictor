Iqwal In Kuala Lumpur Orchestra Concert (Songs Of Asia)

Performances by Azlina Aziz, Liza Aziz, IQWAL (Malaysia), Gina(Philipine), Fahmy Al Makawy (Indonesia),etc.



IQWAL HAFIZ a young thriving artiste, only 16 years old has rise into fame locally and internationally. The success of this young, talented and energetic artiste began his journey into the world of show business when he was only 4 years old. The dramatic rise of this super young talent has been breathtaking.

He had recently; make Malaysia proud by winning 4 Gold Medals, 1 Silver Medal and 1 Hollywood Industry Award in the recent WORLD CHAMPIONSHIP OF PERFORMING ARTS in Los Angeles. WCOPA participated by 5,000 over participants from 48 countries all over the world.

Through his history of performance IQWAL had been selected twice to represent Malaysia in World Championship of Performing Arts. He conquered all 5 vocal categories which includes Pop, Rock, Contemporary, Open Style, Original Works and walk away proud with 4 Gold medal, 1 Silver medal and 1 Hollywood Industry Award for the Malaysian team.

In August 2008, again, IQWAL was invited to represent Malaysia to "The Best New Talent Award" in Kodak Theatre, Los Angeles. Here, IQWAL was crowned as "THE BEST INTERNATIONAL ARTIST" . 
IQWAL went straight to be the best defeating all participants from all over the world such as United States Of America, Philipines, Guatemala, Africa, Mexico & Jamaica to name a few. 

He had created his mark by being the first Malaysian to perform at Kodak Theatre, Los Angeles. 


This artistic young boy, could not only dance well but sings in 14 languages, Italian, Spanish, Mexican Latin, Tagalog (Philipines), Arabic, Hindi, Thai, Korean, Japanese, Cantonese, Mandarin, Tamil, Malay & English

His first attempt into singing career started when he was 4 years old when his mom (Normin Shah Khairuddin @ Myn) first discovered his exceptional talent. Support from Ali Mpire (Ex-XPDC Vocalist) & Mel WINGS (EX-WINGS Vocalist). His hardworking mom, Myn had exposed him to International music, singing competitions and enrolled him in a prominent music school to polish his skills.

Since then he had performed along side famous artiste like M Nasir,  Ramli Sarip,  Zainal Abidin, Faizal Tahir, Nash, Aishah, Mawi  in various concerts such as Citrawarna Malaysia, Malaysian Water Festival, Citrarasa Malaysia, Festival Gendang Malaysia, Di Ambang Merdeka and Masrah Hijrah Concert to name a few. 

He performs for most of  BERITA HARIAN & HARIAN METRO event such as, KARNIVAL JOM HEBOH TV3, ANUGERAH BINTANG POPULAR, FIESTA MEDIA IDOLA, GP JORAN & PERKAMPUNGAN HADHARI as resident artiste since he was 11 years old till now.

Iqwal, also composes songs, since he was 12 years old. One of which he performs with Dayang Nabila a song called 'SECANGKIR KASIH'. 

Young and vibrant, IQWAL without doubt will give a superb performance and leave his crowd content.
 
For more inquiries please contact : 
Myn (Empire All Star Event) at  

019-2002028
  
012-3555 200 

0 3-  7 8 4 2  2 0 1 5 (Office)