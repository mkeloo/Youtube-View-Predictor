Sung in the style of Ethel Merman...more or less.  With many apologies to Cole Porter.

(To the tune of "Anything Goes")
There's a pony who once was singular
But now we can't get rid of her
Mindless drones
Pinkie Pie clones

We all liked to have lots of fun with her
But only when there's one of her
Hold the phone
Pinkie Pie clones

The world has gone pink today
Least we think today
Now there are a few
More like twenty-two
Making a scary face
Playing a game of chase
We wish they'd leave us alone

Though Pinkie Pie is quite, quite scrupulous
Suddenly she's quadrupulous
Who'd have known?
Pinkie Pie clones

Well, there's a magic pool that's under there
Pinkie Pie said a spell and her
Ranks have grown
Pinkie Pie clones

Suddenly right before all of our eyes
We got a wave of Pinkie Pies
Of our own
Pinkie Pie clones

We started with one today
Wanted fun today
Now there's two today
Much to do today
Now there's three and four
And then a whole lot more
Double digits make us moan

They're rampaging around poor Ponyville
Everypony has had their fill
We all groan
Pinkie Pie clones

We could deal with one, maybe two of her
We don't know what to do with her
Wrecking homes
Pinkie Pie clones

We hope this isn't our eternity
Having all them all perminately
Set in stone
Pinkie Pie clones

We're hiding in trees today
On our knees today
Though they're cute, of course
We need excessive force
There's just too much pink
That's at least what I think
They need to leave us alone

We might all have to up and leave this place
We'll call it a hazardous space
Danger zone
Pinkie Pie clones