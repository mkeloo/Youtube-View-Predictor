This production, by Colin Graham, first saw the light in 1966 and was revived in 1984 for the ENO tour of the USA which culminated with two ecstatically received performances at The Met.

GLORIANA
Benjamin Britten
Libretto by William Plomer

Queen Elizabeth I - Sarah Walker
Robert Devereux, Earl of Essex - Anthony Rolfe Johnson
Frances, Countess of Essex - Jean Rigby
Sir Walter Raleigh - Richard van Allan
Penelope, Lady Rich - Elizabeth Vaughan
Sir Robert Cecil - Alan Opie
Charles Blount, Lord Mountjoy - Neil Howlett
Henry Cuffe - Malcolm Donnelly
Lady-in-Waiting - Lynda Russell
A Blind Ballad Singer - Norman Bailey
The Recorder of Norwich - Dennis Wicks
A  Housewife - Shelagh Squires
Master of Ceremonies - Alan Woodrow
A Morris Dancer - Robert Huguenim
The City Cryer - Leigh Maurice
The Spirit of the Masque - Adrian Martin
Time - Ian Stewart
Concord - Amanda Maxwell

Directed for the stage by Colin Graham
Directed for TV by Derek Bailey
Chorus and Orchestra of the English National Opera
Conducted by Mark Elder