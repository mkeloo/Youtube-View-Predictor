()!()!()!()!()!()!

Thank you to everyone who let me film them over the weekend! If you got a page or something, tell me and I will add it to the description! ^_^

Song: We are stars by: Virginia to Vegas

I gotta say this Anime North was the best ever for me, out of the four years I've been going. It was the first time I ever went there for all three days and filmed there too. I'm not regretting it one bit! This year at Anime North I got to experience all sorts of different things. I had a staff pass at Anime North courtesy of Cafe Delish, and omg its so cool, you get to go to the staff lounge and everything *_* I also filmed for the maid cafe, called Cafe Delish, and lol I had to listen to their dance routines so many times I got the songs stuck in my head! It was really cool filming for them, they're a bunch of fun people and if they are still at Anime North next year you should really check them out! 

I couldn't have made this video without my friends who helped me get to Anime North and back, the wonderful person who recommended me to Cafe Delish, my photographer friends who let me shoot along side them, the people who helped me out of my SD card crisis, and of course all the cosplayers! This video holds so much memories for me, and omg editing it was just as fun as filming it cause I would remember back to those fun moments and smile. I can't wait for next year! ^_^

Cosplayer Links

Leona - https://www.facebook.com/TTCRH
Sinon - https://www.facebook.com/AuuDesu
Animegao - https://www.youtube.com/channel/UCB0-1YuBFSL9reBz8Eq8wHQ
Rensouhou-chan Little bot - https://www.youtube.com/user/MrSmartcable
Ruby - https://www.facebook.com/EuphonicCosplay
Flight of the Flute - https://www.facebook.com/Ellexcosplay
Queen Annie - https://www.facebook.com/ForeverYoungCosplay
Gray Fullbuster - https://twitter.com/DatV13tGuy
Maids - https://www.facebook.com/cafedelish
Aether Wing Kayle - https://www.facebook.com/ashecosplays
Snowstorm Sivir - https://www.facebook.com/jodecicosplay
Rikku - https://www.facebook.com/LexxieCosplay
Yoko - https://www.facebook.com/BamzyCosplay
Boss lady in the wheelchair - https://www.facebook.com/DigiRinCosplay
Videographer - https://www.facebook.com/DonDolcePhotography
Photographer - https://www.facebook.com/herbiecidez

And remember, you're all stars :)

Check out other Anime North videos!

https://www.youtube.com/watch?v=MsaxfmOhJSQ Done by Caveman Z
https://www.youtube.com/watch?v=as0tSTkJXco&feature=youtu.be Done by TheUndeadTV
https://www.youtube.com/watch?v=DR_k9SDT_dU Done by DonDolce the best duck ever