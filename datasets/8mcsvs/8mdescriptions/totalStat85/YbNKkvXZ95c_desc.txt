Personalize your gift for a wedding or bridal shower this summer with this handmade gift bag.  Create a beautiful wedding dress on the front of the bag using a circle punch and some pearl stickers. Every bride will love it! 

Brought to you by Martha Stewart: http://www.marthastewart.com

Subscribe for more Martha now!: http://full.sc/PtJ6Uo 

---------------------------------------------------------------

Want more Martha? 
Twitter: http://twitter.com/marthastewart
Facebook: https://www.facebook.com/MarthaStewartLiving
Pinterest: http://pinterest.com/ms_living/
Google Plus: https://plus.google.com/+MarthaStewart/posts 
Martha's Official Blog: http://www.themarthablog.com/

The Martha Stewart channel offers inspiration and ideas for creative living. Use our trusted recipes and how-tos, and crafts, entertaining, and holiday projects to enrich your life.

Wedding Dress Gift Bags - Handmade Home - Martha Stewart
http://www.youtube.com/user/MarthaStewart