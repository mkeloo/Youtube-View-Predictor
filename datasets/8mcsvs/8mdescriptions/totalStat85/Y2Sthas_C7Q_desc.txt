The REFERENCE500s amplifiers employ an extremely efficient
Auto High Current power supply (patent pending). This new power supply
circuitry automatically customizes your amplifier for optimum efficiency and
power output into virtually any impedance load. When other brand amplifiers
are driven at low impedances (i.e., 1 ohm or less), they shut down, squash
dynamics and power output (called current limiting), or waste huge amounts of
power (i.e., low efficiency). All of which reduce the "realworld" power the
amplifier can produce in the car. Soundstream's Auto High Current power
supply allows the REFERENCE amplifiers to be one of two types of amps:
either producing maximum power at higher impedances (perfect for satellites)
or at lower impedances (usually with multiple subwoofers). This is done by
letting the amplifiers' power supply continuously monitor the impedance of the
load the amplifier is driving. If the impedance drops too low, the power supply
will automatically switch into High Current mode. It will stay in this mode until
the amplifier is turned off. The next time it is powered up, it will be in the High
Power mode.
Unlike other amplifiers, Soundstream's REFERENCE amplifiers can be
configured to drive virtually any impedance and make maximum power! The
major advantages of this power supply are:
 awesome dynamic power capabilities
 added continuous power with higher voltages
 increased amplifier efficiency and reliability
Because of the dynamic properties of most music, all audio components
should be able to react accordingly. Thanks to their unique power supplies
Your REFERENCE amplifier is protected against both overheating and short
circuits by means of the following circuits:
 Main power supply fuses
 Auto High Current power supply
 Smart Power Supply Thermal Rollback activating at 85C
 A fail-safe thermal protection circuit activating at 95C
drive virtually any loadall the way down to 1/2 ohm stereo (1 ohm
mono).
 Dual Discrete Class A Drive Stages - Over six times the drive
current of most amplifiers in the Reference500s and 700s, and over
twelve times in the Reference1000s! More drive current maintains
the amplifiers' performance into low impedance loads.
 Drive DelayTM Muted Turn-on/off Circuit - A unique circuit which
completely eliminates any amplifier-related turn-on/off noises.
 Flexible Dual Input Level Sensitivity accepts 2 voltage ranges;
from 200 mV to 2.0 V and from 500 mV to 5.0 V, permitting
maximum output from the amplifier with virtually any source unit.
 Differential Balanced Input Design for added immunity to noise
caused by component and vehicle electrical system interaction when
using Unbalanced RCA inputs.
 True Balanced Input for professional-quality performance and noise
cancellation. The 6-pin din plug carries (+) and (-) Signal information
for Left and Right channels, audio ground, and +15 Vdc to operate
the Soundstream BLT Balanced Line Transmitter
Uncompromising Design and Construction including mil-spec
glass epoxy circuit boards and high current custom gold-plated solid
brass connections that will accept up to 4 gauge power/ground wire.
 Auto High Current - Soundstream's newest exclusive circuit which
automatically customizes your amplifier to its particular application
High Current, low impedance loads (multiple subwoofers, less than 2
ohms mono) or High Power, higher impedance loads (2 ohms mono
and up).
 Coherent StereoTM/Mixed Mono selection for either "pure" stereo
operation or mixed mono for simultaneous stereo and mono.
 ChassisinkTM Darlington Power Array - Soundstream's
"overbuilding" of the output section incorporates multiple output
transistors instead of a few for faster, stronger power delivery. The
transistors are sandwiched between the circuit board and the heatsink
in a design called ChassisinkTM to ensure cool, efficient amplifier
operation.
 PowerGrid Power Supply Design - All power supply components
are located near one another, connected by thick, wide PCB traces,
which ensures rapid, high current delivery. The entire power supply is
isolated on one side of the circuit board while the audio stage is
located opposite it, guaranteeing minimal noise.
 Ultra-Low ESR Capacitance Bank - Multiple small input power
capacitors are used to provide a lower ESR (Equivalent Series
Resistance), which means more power in and out faster.
 Smart Thermal Rollback - Most amplifiers shut off when they get too
hot. In the unlikely event the REFERENCE amplifier reaches 85C,
it will gradually roll back its average power (without affecting the
dynamics). Once the amplifier has cooled off, it returns to full power
output. If overheating should continue, a second thermal sensing
protection circuit will shut off the amplifier if the heatsink reaches 95C.
 Fault Monitor LED on the top panel notifies you of blown power
supply fuses.
 1/2 ohm Drive Ability -