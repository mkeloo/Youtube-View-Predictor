More at: http://gamester81.com/
Intro by Vlad Iacob and Jason Heine: http://www.youtube.com/theemureview
Follow me on Facebook: 
Profile page: http://www.facebook.com/Gamester81
Facebook Fan Page: http://www.facebook.com/Gamester81FanPage

Google+: https://plus.google.com/115156165473483227850/posts

Follow me on Twitter: 
http://twitter.com/#!/Gamester81

Follow me on Tumblr: http://gamester81.tumblr.com/

My other channels: 

Gamester81-Rare & Retro:
http://www.youtube.com/user/Gamester81

Gamester81in3D-Game reviews in 3D: 
http://www.youtube.com/user/Gamester81in3D

NEStalgiaholic-70's, 80's, 90's and today memorabilia and more: http://www.youtube.com/user/NEStalgia...

Starwarsnut77-Star Wars & Sci Fi stuff: 
http://www.youtube.com/user/starwarsn...

Allgengamers YouTube Channel:
http://www.youtube.com/user/allgengamers

Listen to my podcast Allgengamers with cohost MetalJesusRocks, PeteDorr, and TheEMUreview: 
http://allgengamers.com

Watch me live on my twitch.tv channel: http://www.twitch.tv/gamester81