http://www.etomusik.de
http://www.cremeroyale.de

Plastic Poetry is a classic new romantic album par excellence. Eto combines his big retro sound with modern elements of the modern club and electro scene. He concentrates on finding simple melodies und sings about friendschip, love, sex and insanity.

It took two years to finish the album, but now its done. In 14 songs eto gives us an impression of his world. In addition to his single release Hideaway the album also includes the mySpace classics Bicurious and Love Crystals. With Bulldozer and Nobody Knows eto show us his sensitive side.

Tracklisting:
01. Hideaway
02. Bicurious
03. Falling
04. Bulldozer
05. Another Night
06. Skip The Track Back
07. Love Crystals
08. Visual
09. Siamese Twins
10. Alien Lover
11. Taste The Difference
12. nobody knows
13. Dumb Symphony
14. Outro

About eto:
Melancholy on electronic beats - 80's synthiepop meets the tricky present. In spite of the plastic programmings and clubby attitude, eto regards himself as a classic songwriter.

Writing, composing or programming - eto does everything by himself. He is a nightowl and spends most of the time in his studio. There he creates high resolution songs about love, life, identity and insanity. All mixed up with a portion of electro, downbeat or minimal.