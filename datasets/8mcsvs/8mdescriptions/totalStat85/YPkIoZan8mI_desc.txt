Happy new year! Thankful for each and every one of you :) Thanks for getting this channel to 20k+ subbies and over 6.2 million views! 
Hope you enjoy the prizes I got together for you! Here are the contest details:


*This contest is INTERNATIONAL!

Theme: Miniature desserts
- 10-15 clay charms total
- Strictly charms only; no figurines. All pieces should have an eyepin in them.
- helper: I'm looking at realistic, detailed desserts! Think cupcakes, fruit tarts, decorated cake slices
- no kawaii faces please!
- if I do not reply to your questions in the comments, it either means they have been answered, or you have not enabled the "reply" option on your google+ account!

Bonus:
Here are some of my realistic desserts tutorials! Include them in your entry for brownie points ;)
- Fruit crepe: http://goo.gl/6RVuAT
- Butterfly cupcake: http://goo.gl/ptSj1N
- Soft-serve cone: http://goo.gl/zq3jYv
- Ice-cream cone: http://goo.gl/ZU5nuf
- Waffles: http://goo.gl/bdaeNk

Rules:
- Like and favourite this video
- Subscribe to both my channels
- Create a video (no slideshows) of your entry and send the link to sweetnessoverloaddd@gmail.com
- Have parents' permission if you're under 18
- Sending your entry if you win is much appreciated, but not compulsory.
- Deadline: March 31, 2015
- Contest will be cancelled/extended if less than 20 entries are received

***THIS CONTEST IS NOW OVER***
Winner:
Polymerclaynemo: https://www.youtube.com/watch?v=pfKn7FB8J7Y

Special mentions (runners up):
Kendall Hoyt: https://www.youtube.com/watch?v=2wfA8TMwjzE
Love XO: https://www.youtube.com/watch?v=jPDmzfo599U

~

SOCIAL MEDIA:
http://instagram.com/sweetnessyt
weit: http://weheartit.com/sugarry

SHOP @
https://carousell.com/sweetness.co
https://www.etsy.com/shop/sweetnessco

AFFILIATES:

Get 10% off your first purchase at Sophie and Toffee: http://sophieandtoffee.refr.cc/3D2P752 (expires 29 Aug, 2017 - visit my latest videos for updated coupon code)

Get your surprise kawaii fix every month! 
http://www.kawaiibox.com/sweetness/r/25/

Get 10 free coupon cash when you join Panda Hall for the first time!
http://www.pandahall.com/

~

Music from:
www.incompetech.com