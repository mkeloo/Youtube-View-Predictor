Please Read Me!
Hey guys! I put together a simple yet chic lookbook for Fall! The items featured in the video are from a new Korean fashion site, www.fashiontoany.com. I am particularly found of this site for their sleek/fashion forward items that are effortlessly clean and structured. They update with new items daily and they house one of the best quality of Korean clothing online. So if you're into sleek, Korean fashion and/or fun leggings for Fall, check them out!
Disclaimer: The featured clothing items are gifted from FashionToAny. 
Instagram: oiseau88
Featured Items
1. Beige Flare Skirt:http://fashiontoany.com/beige-flare-skirt.html
2. Chictopia Coat, Gray:http://fashiontoany.com/chictopia-coat.html
3. Black Flower Dress:http://fashiontoany.com/black-flower-dress.html
4. [Giveaway Item] Warm Knit Top, Gray:http://fashiontoany.com/warm-knit-top.html

FashionToAny Giveaway Rules: 2 winners will receive a pair of leggings of choice and a warm knit top shown in the video.
1. Please be a subscriber to my channel, Oiseau88
2. Please "Like" FashionToAny's Facebook page:https://www.facebook.com/fashiontoany
3. Please comment on this video posted on FashionToAny's Facebook saying that I sent you and/or what you enjoy doing during the fall/winter months (holidays)!
Giveaway Ends November 4th, 2013 and 2 Winners will be announced on their facebook on November 5th, 2013 (Eastern Time)!