In this Beginner German Lesson for Children you will learn the German names for common drinks and snacks you get in a cafe including: pop, cola, fruit juice, water, tea, sandwich, chips (fries), pizza, ice-cream, and cake.

CC closed caption subtitles available to learn German Cafe lesson in many languages.

In the lesson, each word or phrase is shown in German (and English) so you the learner know exactly what you are learning. The German is repeated 3 times so you can clearly remember how to pronounce that vocabulary. You will see the German words on the screen twice so that you will know how to read and spell those words. 

Beginner German Lessons for Children offers 56 free audio and picture lessons for learning basic German words, phrases and sentences. Each of our German lessons uses audio speech and specially drawn pictures to teach kids and young people 10 items of simple German grouped in a topic. Topics include 'Family members', 'Daily routine', 'Fruit', 'Pets', etc.

Check out more German lessons on the German Games channel or stop by www.german-games.net for free interactive lessons, games and tests with all our vocabulary topics.