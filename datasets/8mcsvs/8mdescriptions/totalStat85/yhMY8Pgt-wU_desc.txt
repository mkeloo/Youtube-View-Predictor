Website: http://www.zoomtv.in

Rumours of Vidya Balan's link up with her co-stars have been on from her Parineeta days. But none of it seems to deter this simple beauty.

She quips, "So far I am not actually linked to anyone. So, maybe that's why they are trying various permutation combinations to see if any of them work."

As for working with so many big banners, she says, "Honestly after I did Parineeta I thought I'll never get such a wonderful work environment. It was home ground for me. But even after working out of Vidhu Vinod Chopra productions, I have been working with wonderful people. I am just so glad."

After the August release of Hey Baby, Vidya Balan will be seen in Bhool Bhulaiya and Hulla Bol.  

Model Urvashi Sharma has finally come out of the closet after keeping away from the limelight for her special launch in the film Naqaab.