http://www.skaterhq.com.au

Check out the full line of scooter parts in the limited edition "Skullaz" colourway that just arrived in from District! Skater HQ brings you this time-lapse of a complete scooter build using all the Skullaz components. 

-----

Check http://www.skaterhq.com.au for all the latest products.

Follow us on Twitter https://twitter.com/SkaterHQ

Like us on Facebook http://www.facebook.com/pages/SkaterHQ/119058454792635

-----