How 2 Make and Use Paper Mache Pulp

You are going to love this pulp for its smooth fine qualities. You can render endless little sculptures with mache pulp that would otherwise be difficult. 

I love to make beads in all different sizes and shapes. I also find pulp an excellent choice for sculpture details.

I am going to show you 2 easy ways to create your own homemade pulp.

Visit our studio to read the full article: http://whimsypapermache.blogspot.com/2012/02/how-2-make-and-use-paper-mache-pulp.html

Thank you for stopping by our whimsy art studio.