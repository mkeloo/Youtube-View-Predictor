Sega Rally 2 (2) is an arcade racing game developed by Sega AM5 for the Model 3 arcade hardware. It is the sequel to 1994's Sega Rally Championship. Sega Rally 2 was first released in arcades in February 1998, and was later ported by Smilebit to the Sega Dreamcast, becoming one of the console's earliest titles when it was released in Japan on January 28, 1999. 

Captured from a real Dreamcast (no emulation) at 60FPS using vga box, hdmi converter, upscaled to 1080P by software. Japanese Version (voice and subs)

Follow-Us: www.twitter.com/garchiveHD
Subscribe: http://www.youtube.com/user/garchivehd?sub_confirmation=1

Gameplay by: Velberan
www.youtube.com/velberan