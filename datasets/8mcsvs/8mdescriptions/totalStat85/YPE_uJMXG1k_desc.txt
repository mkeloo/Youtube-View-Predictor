Preorder Now at
GameStop UK        http://ow.ly/n7OMA
GameStop Ireland   http://ow.ly/n7OIH

Disney Interactive released a new Behind the Scenes video for Disney Infinity. It focuses on the Toy Box mode. The game releases on the 22nd August.

 The Disney Infinity Starter Pack includes:

    Disney Infinity video game
    Disney Infinity Base
    3 Disney Infinity Figures: Mr. Incredible, Jack Sparrow, and Sulley
    3 Disney Infinity Play sets: Monsters University, Pirates of the Caribbean, and The Incredibles
    Your first Disney Infinity Power Disc
    Unique web codes to unlock content online

Disney Infinity Overview

Introducing DISNEY INFINITY, a new video game where a spark of imagination unlocks the freedom to play with some of your favourite Disney and Disney/Pixar worlds like never before!

Play in their worlds. Place your Disney Infinity Figures onto the Disney Infinity Base to jump into a Disney Infinity Play-Set and experience original adventures in the worlds of Monsters University, Pirates of the Caribbean and The Incredibles.

Take on the role of Sulley, the naturally gifted 'scarer'; become Captain Jack Sparrow, the sword-wielding pirate; or transform yourself into Mr. Incredible, one of the world's greatest crime-fighters.

Battle enemies, solve puzzles, overcome obstacles and complete a variety of other unique quests.

Or create your own world! Unlock virtual toys from each Disney Infinity Play-Set -- characters, buildings, weapons, gadgets and more -- and bring them into the Disney Infinity Toy Box where you can mix them all up to create your own game.

In the Disney Infinity Toy Box, there are no rules and you can create any adventure you want. It's up to you and your imagination!

Share your one-of-a-kind creations with your friends with up to 4-player co-op play!

Collect more. The more you play, the more you unlock. And the more you have, the more possibilities for play!

You can also add new Disney Infinity Play-Sets and Figures. Or take your Disney Infinity experience to a whole new level with Disney Infinity Power Discs -- an all new way to unlock character powers, fun gadgets and new ways to customize your world.

The story never ends. The Disney Infinity Starter-Pack is just the beginning, with many more Disney and Disney/Pixar characters and worlds coming soon, giving you infinite ways to inspire your imagination!

 KEY FEATURES:

    Unprecedented Creative Play - Featuring a virtual "Toy Box" and a vast open world environment, players can create their own stories and customize their adventures combining characters, environments, gadgets, vehicles and more.  The possibilities are truly infinite as Disney will add new additions from the company's vast library of entertainment properties.

    Infinite Storyline Possibilities -- Disney prides itself on its storytelling culture and for the first time ever, Disney Infinity will put the story creation into the hands of consumers. Players will be able to create their own adventures in an open-world environment and will give players unprecedented freedom for creative play.

    Multiple Franchises Across All Platforms -- Disney Infinity introduces an all-new universe, featuring a toy-inspired art style where multiple Disney franchises and characters can exist and interact in one game and across Xbox360, PS3, Wii, Wii-U, 3DS, PC and online, as well as on smart devices including tablets and mobile phones.

    Interactive Figures Activate In-Game Adventures and Customizations - Disney Infinity will initially introduce a line of 40 collectible interactive figures - characters, "Play Set" pieces and power discs - that allow players to expand and customize their play experiences. Over time, Disney Infinity will release additional characters and "Play Sets" and this platform will constantly evolve.
    17 interactive character figures allow players to experience a variety of their favourite characters in each "Play Set" and in "Toy Box" play
    3 "Play Set" pieces that add new Disney and DisneyPixar "Play Set" locations
    20 power discs enhance environments, add gadgets and more

    Co-op Multiplayer Action - Disney Infinity supports 2 player co-op play in structured adventures and up to 4 players in "Toy Box" mode.