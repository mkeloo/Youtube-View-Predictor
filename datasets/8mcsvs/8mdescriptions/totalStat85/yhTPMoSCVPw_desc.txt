Original digital video content created by Sheri Rose Shepherd. Copyright 2013, Sheri Rose Shepherd. Used by permission of His Princess Ministries. All rights reserved.

You can find more devotionals by Sheri Rose Shepherd here: http://www.biblegateway.com/bible-life-coaching/

Sign up to receive Sheri's weekly devotional vie email: http://www.biblegateway.com/newsletters/