UPDATE! I am aware that during some parts of the video, the fps gets hung up for a bit and makes the image choppy.  I'm going to mess around with some different export and upload settings to see if that will fix the problem.  Sorry for the annoyance until I get it fixed, but thanks for watching!

Just a quick little sample of what the Letus35 Mini DOF adapter does, for those interested.  This was shot very quickly and sloppy (didn't use the tripod, sorry for shaky shots) and edited quick as well, with just some slight color correction and a little color grading in Final Cut Pro X.  Not the best quality video, but it serves it's purpose as a demo and a test.
Gear:
-Canon Vixia HF R20 HD camcorder
-Letus35 Mini DOF Adapter
-Canon EF 50mm f/1.8 II Lens
-Cavision Rod Support System

Camera Settings:
-Manual focus(which is focused on the Letus's ground glass element
-Cinemode
-PF24 frame-rate
-Custom white balance
-MXP
If you have any questions, leave a comment or send me a private message.  
Thank you for watching and please subscribe!  There will be plenty more to come very soon, so keep an eye out!

Music- "You And I" by Ingrid Michaelson