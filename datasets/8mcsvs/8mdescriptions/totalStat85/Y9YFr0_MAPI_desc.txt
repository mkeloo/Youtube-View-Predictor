Ah, at last, the finale!  And a lovely one at that!  Yes, even though some of Gertrude's lines in here would have fit Fluttershy better, I actually had finale-ish clips of AppleSpike to work with.
The end is based on the production when both worlds come together, in case you're wondering.

Cast:
Horton the Elephant - Spike
Gertrude McFuzz - Applejack
Sour Kangaroo - Ms. Harshwhinny
Young Kangaroo - Diamond Tiara
The Cat in the Hat - Pinkie Pie
Jojo - Applebloom
Mayor - Big Mac
Mrs. Mayor - Cheerilee
Elephant Bird - Peewee