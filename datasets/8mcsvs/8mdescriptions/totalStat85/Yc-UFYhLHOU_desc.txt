DOWNLOAD "studio" version:
http://www.jaybmusic.net/download.php?id=78

REMIX PACK: http://www.jaybmusic.net/Remixes/RisingSun2015_RemixPack.rar

SUPPORT AND DONATE: 
https://www.paypal.com/us/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=QZXUK7PGZHKEW

You will notice a slight difference between this live video and the previous ones. Not only have I invited again my evil twin brother JayC, also my cousin JayG came over to play some guitar for us. Unfortunately we didn't have a chair for him so he needed to sit on the floor. 
The other thing that's new is that I don't use pure-hardware drums in this video, in fact I get a lot of support from the PC here. Thus I can make this track sound much more modern than the previous live performances. I hope you don't mind the slight change of style. I love it.

This is BTW an old track of mine from 2006, which can still be downloaded from my website as well. It has always been one of my favorites of that time. 
If the title "Rising Sun" awakes the wish in you to make a video about Princess Celestia, please do so. I don't mind. But please use the download version of that song, not the live version. Anyway, this is not a pony song.
_________________________________

Audio equipment:
- Yamaha AN1x: "DirrtyBass" (Bass)
- Yamaha S90 ES: "Trance Inc." (Pad)
- Korg X5D: "Very Thick" (More Pad)
- Quasimidi Sirius: "Spectral", "SFX 1", "SlowStrg", 909 cymbals and more hihats
- Roland V-Synth XT: "JayB's Grand" (piano)
- Yamaha QY100: bass slide, seashore, 909 drums
- Epiphone Les Paul (yeah, the guitar part...)
- RevealSound Spire (the additional basses)
- Logic EXS24 (drums, claps, snares and more drums)

Most transition effects are part of the "JayB's effect sounds" pack, which can be downloaded from my website. For free.
______________________________

JayB homepage: http://www.jaybmusic.net/
JayB on Facebook: http://www.facebook.com/jaybmusic
JayB on Twitter: http://twitter.com/jaybproductions
JayB on SoundCloud: http://www.soundcloud.com/jaybmusicnet
JayB on Tumblr: http://jaybmusic.tumblr.com/