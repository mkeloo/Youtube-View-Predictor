This video shows one technique for getting fresh drinking water from a wild grape vine.  Being able to get fresh drinking water in a wilderness survival situation is a critical survival skill to have! If you don't know several ways to get water in the wilderness you should study up on it! Take a look at this Playlist: https://www.youtube.com/playlist?list=PLi9Gy0TQkj_rRy-P8rEqRQdz0rERWhIu7

Please add me to your Google + Circles!  https://www.google.com/+RealitySurvival

Checkout my new Survival Gear Store on my website at: http://www.realitysurvival.com/survival-gear-sale/

Welcome to the Reality Survival Channel! On this survival channel I teach a full range of emergency Wilderness Survival Skills and Urban Survival Skills, as well as Primitive Survival Skills.  My goal is to educate folks on real world survival tips and survival techniques that cover the gambit of emergency Wilderness Survival, Urban Survival, Primitive Survival, Bushcraft, Disaster Preparedness ( Prepping ) & more. I also do Outdoor Gear Reviews on Survival Kits, Survival Gear, Survival Knives, Guns, Bug Out Bag Gear & just about any kind of Every Day Carry (EDC) Gear & disaster preparedness or prepping gear.  I am a prepper & also do some DIY & How to videos on prepping as well as teaching different Survival skills, bushcraft skills, & prepper or prepping skills to help you sort out what really works & what doesn't.  I learned wilderness survival skills when I was a Survival Evasion Resistance and Escape (SERE) Wilderness Survival Instructor for the USAF about 15 years ago (class 97-02). On the Reality Survival Channel you will typically see a mix of survival & prepping & gear review videos each week. I usually upload at least one survival skill video, one gear review video, one prepping video every week. Typically uploads happen on Wed, Sat & Sun mornings.

You can subscribe to the Reality Survival Channel here: http://www.youtube.com/subscription_center?add_user=RealitySurvival  

Also be sure to visit my Survival blog at: http://www.RealitySurvival.Com/  where there are many articles on survival, survival skills, prepping, gear reviews and more!

Here are a couple great Survival video playlists you may also enjoy!

Survival Kit Videos: http://www.youtube.com/playlist?list=PLi9Gy0TQkj_o40UvW9z5e3QtziMk9mBY1 

Cool Survival Tricks & Tips: http://www.youtube.com/playlist?list=PLi9Gy0TQkj_re7im2kUb1p2-o1lBBto2x  

Who am I? I ( JJ Johnson ) am a former active duty USAF SERE Instructor.  What is a SERE Instructor ? SERE stands for Survival Evasion Resistance and Escape.  Basically it was a position that taught combat Wilderness Survival techniques to USAF aircrew members in the mountains North of Spokane Washington near Fairchild AFB.  I am also the owner and author of http://www.RealitySurvival.Com.  I have worked for the DoD in various positions for about 17 years now.  I am an avid outdoorsman, hunter, shooter, and gear reviewer, a prepper, a Christian and a conservative constitutionalist.   

You can connect with me on the following social media pages if you would like! For all of 2014 I am doing a monthly giveaway for those subscribers that share, comment, playlist, favorite and like my posts regularly!  Be sure to get in on it and let me know you are sharing by connecting with me on social media below.

TWITTER: https://twitter.com/RealitySurvival or @RealitySurvival 

FACEBOOK Personal profile: https://www.facebook.com/JJatRealitySurvival 

FACEBOOK Page: https://www.facebook.com/RealitySurvival 

TUMBLR: http://realitysurvival.tumblr.com

STUMBLEUPON: http://www.stumbleupon.com/stumbler/RealitySurvival 

LINKEDIN: http://www.linkedin.com/in/realitysurvival 

INSTAGRAM: http://www.instagram.com/realitysurvival/ 

FACEBOOK GROUP:  
Prepper Skills: https://www.facebook.com/groups/PrepperSkills/ 

INSTAGRAM: 
http://www.instagram.com/RealitySurvival

Royalty free music provided by Youtube.