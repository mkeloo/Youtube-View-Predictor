I made this bug bivy bug net for a total of $5.87.  It took me just a few minutes to put it together.  

The bug net is actually a sheer curtain I purchased from Walmart for $4.87.  They had plenty of them in a few different colors.  Get black, it works best when using your flashlight at night to look through.  Another person on Youtube measured the holes in the fabric in this curtain and determined that it will keep out noseeums. The plastic part is plastic film you use to cover your windows in winter.  You can get it at most dollar stores for $1.

To put it together, I taped together two plastic sheets for the bottom, and taped the curtain on as the top.  I left an opening on the head end to form an entryway.  I put a rope around the top edge so I can cinch the top open/closed as needed. 

I'll answer any questions you might post in the comment section.