Beautiful footage using DJI Phantom 2 with Gopro. Shot at Ovenden Moor Wind Farm, UK. 

Recorded in 2.7k in windy conditions.

Much more aerial drone footage to come so subscribe for more.

Social hangouts:
http://facebook.com/Revtech4k
http://instagram.com/Revtech_4k
https://twitter.com/Revtech4k
https://www.google.com/+Revtech4K