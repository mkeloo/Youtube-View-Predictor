Una canzone e un video in ricordo di Francesco Fornabaio

05/23 l'aereo giallo

Un attimo e poi non pi
e un semplice ciao divenne addio
Lincantesimo cos fin
dietro agli alberi, oltre cui lui svan

Gli acrobati sono angeli che sognano 
e donano le ali a chi volar non pu

Semplici idee che frullano
domande che ti rincorrono
Tutto  magia, quando sei lass 
soprattutto se viaggi a testa in gi

Gli acrobati sono angeli sopra di noi
e volano per sempre, come gli eroi

Ora  silenzio sopra quel campo
ma se alzo gli occhi vedo laereo giallo
lavagna il cielo, un gesso bianco
disegna sogni fermando il tempo

poi vola su, lontano oltre le nuvole
ma una poesia rimarr da scrivere

La libert  unutopia
racchiusa in unacrobazia
ma vincoli io mai ne avr
se penser a quegli air show

Gli acrobati sono angeli che sognano 
e donano le ali a chi volar non pu

Il cielo  un telo che avvolge il mondo
giunta  la sera di un nuovo autunno
Scende il sipario a luci spente
quanti ricordi oltre le spalle
Infondo tutto  un gioco di specchi
ma linfinito sta nei tuoi occhi
E ancora in volo laereo giallo 
per dire che tutto  ancora possibile 
e che la luce  dentro te
e se chiudi gli occhi senti che
 lass che danza tra le nuvole
 lass che danza