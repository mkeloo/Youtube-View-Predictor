***TRANSCRIPT AVAILABLE BELOW***SCROLL DOWN***

If you wish to contribute to Jennifer's participation in this year's Summer Deaf Olympics, Please send your tax deductible check or money order to:

USADSF (USA Deaf Sports Federation)
PO Box 9130338
Lexington, KY 40591-0338
(Memo: Jennifer Kutcka)

Encouragement can be sent to jkutcka@comcast.net

-------------------transcript-------------------------

Hello - I'm Jennifer Kutcka, my sign name is "J". I'm here to let you know I'm on the U.S. Deaf Olympics team. I'm thrilled, and hope to bring home a gold medal, or some medal.

I'm reaching out because each athlete must raise their own funds. The USA Deaf International Olympics fee is $2,350, then there's the round-trip flight from Chicago to Sofia, Bulgaria and training before the competition. We'll have expenses for the hotel, the practice building, food, and of course airline bag fees for the bowling balls, which weigh 14-15 pounds each. New airline regulations and fees raise the flight cost to over $500 each way. With the Olympic fee of $2,350 and other expenses estimated at $2,400, I will need to raise $4,800.

My selection for the team started in August, when I went to Texas for team tryouts. We spent a lot of time bowling. The coach said he would contact me through VP (video phone), and so I waited. In October, I got the call that I didn't make the team. He explained that the U.S. Deaf Olympics bowling team has historically been made up of six men and six women. Due to budget cuts, this year the team size is reduced to four men and four women. Since I placed 5th or 6th, I did not make the cut. I was disappointed, of course, but it was fine. In February, I got a call that the fourth woman stepped down from the team, I wasn't told why. The coach offered me a place on the team, and I said Yes, I WANT in! When I asked how much it would cost, he told me I would need to raise $4,800 in just four months. I nearly dropped my teeth! I'm thinking positively, though, and I believe I will have what I need, with your support. As you reach out to your friends, coworkers, and family, donations from everywhere will supply the funds needed for me to go. I am confident it will happen.

Participating in the Deaf Olympics has been a dream of mine since childhood. I started bowling at 7 years old, my grandmother, mother, family, aunts, uncles, and cousins have been bowlers for several generations, so naturally I started bowling. I wouldn't call myself an expert, but I'm a pretty good bowler. That's why I would love to bring home a gold medal! I'm asking for your support, whether financial, prayers, or an encouraging word. I appreciate any gift, $5, $10, $20, $25, $50, even $100 or more ;-)

Thank you so much for watching. If you want more information, you can go to the website, http://www.usdeafsports.org. The internet has a variety of information such as the different sports, their history, and more. I remember reading that 82 nations will be gathering in Sofia, Bulgaria for the summer Olympics this year. Eighty-two, they say it's historic. It'll be my first experience.

We'll fly out July 19 or 20, as the coach wants us to arrive early for practice. The opening ceremonies will be on July 25, and the Olympic games will continue until August 4. Some may think we'll have time for sightseeing, but not really. When we arrive, we will be focused on training, then there's the competition, awarding of medals, and closing ceremonies. All the while, we'll also be meeting deaf people from around the world. Other countries don't use American Sign Language, they have their own sign languages, so we will be communicating with gestures, signs, and whatever it takes to understand one another. There are sure to be communication breakdowns, but I'm up for the challenge, and will enjoy meeting people from other countries.

Thank you again. I hope to win gold for you all, for Team USA. Love to you all!