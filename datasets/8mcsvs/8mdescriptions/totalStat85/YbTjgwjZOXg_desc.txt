Verbier Festival Orchestra, chamber concert
Beethoven Septet op.20  E flat
Large Concert Hall of the Mozarteum (Salzburg, Austria)

Christoph Koncz, violin.
Lea Boesch viola.
Amandine Lecras, violoncello.
Sebastian Wypych, double bass.
Robindro Nikolic, clarinet.
Emilia Zinko, bassoon.
Andres Bercellini, horn.