Thanks everyone who subscribed!
Feel free to hit that yellow button and those thumbs in the up position.

Does anyone have a solution for the Anti Aliasing in FSRecorder?
Simulator: FS2004
Scenery: Flytampa Seattle-Tacoma International Airport
Airplane: QualityWings 757
Weather: REX2004

Software:
MAGIX Video Pro X3
FSRecorder (for rendering)
Fraps (for audio)

Hardware used in this video:
Trustmaster T.Flight Hotas X