Gary Warburton, Head Teaching Professional at Nudgee Golf Club, demonstrates and defines the fundamentals of the putting stroke and how to remove any wrist breakdown through impact using the "Croker Golf System."

TAKING THE STANCE
The "Address Routine" is a key function that:
1. Sets the club face and body aligned to the path of the putt.
2. Builds consistent and correct posture and balance in the stance to the ball.
3. Allows the mind to become "quiet" over the putt.

BUILDING A SOLID "NO BREAKDOWN" IMPACT ZONE
To allow this action through the "Impact Zone" with the putter to be consistent with both distance and directional control it is important that the body remain stable and in balance and for both the backswing and forward stroke to have the putter head remain close to the ground. 
To do this the left hand "pushes" back against the slight resistance of the right hand in the backswing and the right hand wrist area "pushes" forward and down and out against the slight resistance of the left hand. 
In the forward motion as the right hand "pushes" down and out, both wrists "uncock - thumbs down" through the ball.

"Thumbs Down" through "Impact" is the key as the right arm extends and drives the club shaft into line with the left forearm. The putter head moves close to the ground and it is ideal for the putter head to remain still at the end of the follow through.

Rory McIlroy is a stand out example of maintaining a "THUMBS DOWN" Follow Through.

The "Putting Drill",with the "Yardstick" helps build a correct putting stroke and removes the over thinking that can lead to steering and inconsistent putting.

Building solid putting mechanics is just the beginning of the process to become a super putter.

Distance Control, Reading Greens, The Mental Game and How to develop it, are all required for a golfer to truly become a great putter.

The Croker Golf System addresses all areas and the "Key to Golf Program" has it.
Contact Peter Croker on crokergolfsystem@gmail.com for more information. 
To start improving your golf game no matter what your level is, visit us at http://crokergolfsystem.com and http://keytogolf.com/putting/ .

==============================================================================================

Get personalized coaching and access over 300 training videos inside the Croker Golf System Coaching Community. 

Visit http://crokergolfsystem.com/membership/ for full details.