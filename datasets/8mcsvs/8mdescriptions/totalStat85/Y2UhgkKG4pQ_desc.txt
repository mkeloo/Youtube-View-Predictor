Jesse Rock & Nathan Hanna playing "Banjo Boogie" (based on Norbert Hebert's "Tree Farm Boogie") for some tourists at Perkins Cove in Ogunquit, Maine on 9/16/12.
-----------------------------------------------------
Jesse & Nathan also played "El Cumbanchero" for the tourists:
http://youtu.be/MojIFbgwWOU
-----------------------------------------------------
Tenor Banjo (B&D Montana) ~ Jesse Rock
Plectrum Banjo (Ome) ~ Nathan Hanna
-----------------------------------------------------