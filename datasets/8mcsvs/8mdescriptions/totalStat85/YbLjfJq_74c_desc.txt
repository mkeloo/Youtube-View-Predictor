Reading Music Course (and a whole lot more): https://joeraciti.com
Beginner? Try this new app: http://www.joytunes.com/ytchannels?pid=ytchannels&c=joeraciti
SUBSCRIBE: http://bit.ly/JoeRacitiSubscribe

Patronize Me (seriously, I'd dig that): https://www.patreon.com/JoeRaciti

Learn Piano From Me!: http://www.joeraciti.com

Where Have You Been Sheet Music: http://bit.ly/WhereHaveYouBeenSheetMusic

Vote For The Next Tutorial: http://www.facebook.com/FastPiano
Follow me on Google+!: http://bit.ly/YTPTGooglePlus
My Music!: http://bit.ly/RomanticSharkAttack
T-Shirt Link: http://bit.ly/JoeRacitiTShirts
My Website!: http://joeraciti.com/

Gear:
Studio Projects C1 Large Diaphragm Condensor Microphones (2): http://amzn.to/PxLeJj
Creative Labs E-MU Preamp: http://amzn.to/V1Euaw
Monster Microphone Cables: http://amzn.to/V1EF5M
Senheiser HD 280 Pro Headphones: http://amzn.to/O7NETU
Keyboard: I use a really old Roland keyboard that I don't think they even sell anymore. I've heard good things about this keyboard: http://amzn.to/S2ecFg - I recommend trying out whatever keyboard you are thinking about in a store before buying.
Camera: I currently use a Kodak Zi8: http://amzn.to/PxNvV0 - you might also try a later model if you are interested in this camera and price point... maybe something like the Kodak Playsport: http://amzn.to/S2fi3Y
I edit my videos on an Asus U46E-BAL7 computer: http://amzn.to/OyZvKw using Adobe Premier Pro CS6: http://amzn.to/UcwGDx
I edit audio using Reaper: http://www.reaper.fm/

In this piano tutorial I show you how to play "Where Have You Been" by Rihanna. You can find my other tutorials by searching the name of the song you want to play + "Joe Raciti" or "jraciti1". If you would like different piano lessons from the ones I have posted, you can submit a request at the "Request A Tutorial" link above. Otherwise, leave a comment in the comment section or thumbs up an existing comment to let me know what songs you would like me to teach you. I can't promise that I'll be able to get to them but I will at least consider your recommendations.

My free online lessons are made with love and don't cost you a dime so please, let me know you appreciate what I do by leaving a comment, favoriting, sharing, and subscribing.

Where Have You Been Lyrics:

[Rihanna]
I've been everywhere, man
Looking for someone
Someone who can please me
Love me all night long
I've been everywhere, man
Looking for you babe
Looking for you babe
Searching for you babe

Where have you been
Cause I never see you out
Are you hiding from me, yeah?
Somewhere in the crowd

[Hook]
Where have you been,
All my life, all my life
Where have you been, all my life
Where have you been, all my life
Where have you been, all my life
Where have you been, all my life

[Beat Break]

[Rihanna]
I've been everywhere, man
Looking for someone
Someone who can please me
Love me all night long
I've been everywhere, man
Looking for you babe
Looking for you babe
Searching for you babe

Where have you been
Cause I never see you out
Are you hiding from me, yeah?
Somewhere in the crowd

[Hook]
Where have you been,
All my life, all my life
Where have you been, all my life
Where have you been, all my life
Where have you been, all my life
Where have you been, all my life

[Beat Break]

Where have you been, all my life

[Bridge]
You can have me all you want
Any way, any day
To show me where you are tonight

[Outro]
I've been everywhere, man
Looking for someone
Someone who can please me
Love me all night long
I've been everywhere, man
Looking for you babe
Looking for you babe
Searching for you babe
I joined GuruFocus.com for my stock research. Great screener, charts and fundamental data: https://www.gurufocus.com