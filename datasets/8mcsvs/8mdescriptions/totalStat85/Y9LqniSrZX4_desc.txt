This review was filmed in May 2014 with staff from Ellis Brigham stores across the UK who were in Hinterux, Tirol, Austria to test 2014/15 skis in ideal conditions.

This was one of pleasant surprises during testing, quite how Nordica can put so much performance into a ski of this price. Its easily the most impressive 500 piste ski weve ever tried. Wood and a layer of titanal provides the grip and precision of a ski much more expensive, with energy to burn too. Yet the handling is never difficult and the shape ensures the ride is silky smooth.

Tester Comments: Rob (Manchester Deansgate) Awesome grip throughout the turn; loads of power and rebound from tail. Amazing price for the performance.

Radius: 14m (168cm)
Sidecut: 122.5/72/105.5 (168cm)
Sizes: 160, 168, 176

View this ski here: http://www.ellis-brigham.com/products/nordica/spitfire-ti-skis--n-pro-evo-bindings-1415/700450