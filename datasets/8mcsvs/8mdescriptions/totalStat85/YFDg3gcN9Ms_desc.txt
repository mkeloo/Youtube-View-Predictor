Join Oxford Motorcars as we introduce you to our MGA Twin Cam race car that has been re-engineered to realize the goals set by the MG Competition Department sixty years ago this year when they sent the EX182 cars to Le Mans in 1955. We take the car on a road test and put it through its paces! Join us at the 2015 Fall Festival at Lime Rock Park to see the car's debut on the track.

Shot and edited by Nick Piccirilli.

Music:
"Welcome to the Show" Kevin MacLeod (incompetech.com) 
Licensed under Creative Commons: By Attribution 3.0
http://creativecommons.org/licenses/by/3.0/