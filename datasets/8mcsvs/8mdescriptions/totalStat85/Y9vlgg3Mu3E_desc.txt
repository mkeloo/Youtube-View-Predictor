The Xbox Minecraft 1.8.2 update includes Creative Mode, sprint, new biomes, hunger bar, stackable food, food munching sound, chicken, rotten flesh, renting servers and MUCH MORE!

Full List of 1.8.2 Update Changes: http://www.minecraftwiki.net/wiki/Version_history/Beta

4J Studios Tweeted "We're working on the 1.8.2 update, and deciding what to bring forward from 1.9 and beyond. Many weeks of work to do yet, so not out soon." 

@TrueTriz
Twitch.TV/TrueTriz

Creative Mode is being built into Minecraft for the Xbox 360 which will be totally awesome!