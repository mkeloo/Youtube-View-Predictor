Das sterreichische Bundesheer lud auf der Hauptstrae in Kobersdorf zur Angelobung am 28. November 2008.

Die Marktgemeinde Kobersdorf und das Militrkommando Burgenland
erlauben sich, Sie zur
feierlichen Angelobung von 350 Rekruten
am Freitag, dem 28. November 2008, um 15 Uhr
am Hauptplatz Kobersdorf einzuladen.

Das Programm sieht folgendermaen aus (Beginn 15:00 Uhr):
- Ankndigungsignal
- Meldung an den militrisch Hchstanwesenden
- Bundeshymne
- Abschreiten der Front
- Flaggenparade
- Begrung durch den Brgermeister
- Musikstck
- Worte der militrischen Geistlichkeit
- Musikstck
- Ansprache des militrisch Hchstanwesenden
- Ansprache des zivilen Hchstanwesenden
- Musikstck
- Angelobung der Rekruten
- Landeshymne
- Erbitten weiterer Befehle
- Abrcken

Source: http://www.ceea.at