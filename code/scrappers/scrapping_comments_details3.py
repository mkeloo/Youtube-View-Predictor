import csv
import requests
import json

key = "AIzaSyAjkrSgzR6voSP3ut1Mj1ukkAZ_xYtEC_0"

totalStatsFD = open('../datasets/8mcsvs/8mmerged/totalData3.csv', 'r')
totalStatsBridge = csv.DictReader(totalStatsFD)
opfile_root = "../datasets/8mcsvs/8mcomments/totalComments3/"
i = 0
for videoStats in totalStatsBridge:
    i = i + 1
    url = "https://www.googleapis.com/youtube/v3/commentThreads?part=snippet&videoId="+videoStats['videoId']+ \
      "&key="+key+"&textFormat=plainText&"
    opfile_full = opfile_root + videoStats['videoId'] + ".txt"
    fileFD = open(opfile_full, 'w')
    r = requests.get(url=url)
    if r.status_code != 200:
        opdata = "kingini, mingini\n"
        fileFD.write(opdata)
        print("Lolcats")
        print(i)
        continue

    commentData_raw = r.json()
    if 'items' not in commentData_raw.keys():
        opdata = "kingini, mingini\n"
        fileFD.write(opdata)
        print(i)
        continue

    commentData = commentData_raw['items']

    if not commentData:
        opdata = "kingini, mingini\n"
        fileFD.write(opdata)
        print(i)
        continue

    for comment in commentData:
        if 'snippet' not in comment.keys() and 'topLevelComment' not in comment['snippet'].keys() and \
                        'snippet' not in comment['snippet']['topLevelComment'].keys() and 'textOriginal' not in \
                comment['snippet']['topLevelComment']['snippet'].keys():
            opdata = "kingini, mingini\n"
            fileFD.write(opdata)
            print(i)
            continue
        else:
            opdata = comment['snippet']['topLevelComment']['snippet']['textOriginal']
            opdata = opdata.encode('ascii', 'ignore').decode('ascii')
            fileFD.write(opdata)
    print(i)