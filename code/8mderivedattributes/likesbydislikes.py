import glob
import csv

fileList = glob.glob('../datasets/8mcsvs/8mmerged/*.csv')
print(fileList)
for file in fileList:
    videoStatFD = open(file,'r')
    videoStatBridge = csv.DictReader(videoStatFD)

    fileName = file.split('/')
    impPart = fileName[4][-6:]
    if impPart[0] == 'a':
        impPart = impPart[-5:]
    print(impPart)

    writeFilePath = '../datasets/8mcsvs/8mlikesbydislikes/likesbydislikes'+impPart
    print("Starting "+ writeFilePath)
    likesByDislikesFD = open(writeFilePath, 'w')
    headers = ("videoId", "likes/dislikes")
    likesByDislikesBridge = csv.DictWriter(likesByDislikesFD, headers)
    likesByDislikesBridge.writeheader()
    lbdDict = {}
    for videoStat in videoStatBridge:
        lbdDict = {}
        lbdDict["videoId"] = videoStat['videoId']
        if int(videoStat['videoLikeCount']) <= 1000:
            lbdDict["likes/dislikes"] = -2

        elif int(videoStat['videoDislikeCount']) != 0 and int(videoStat['videoDislikeCount']) != -1 and \
                        int(videoStat['videoLikeCount']) != -1 and int(videoStat['videoLikeCount']) >= 1000:
            lbdDict["likes/dislikes"] = float(int(videoStat['videoLikeCount']) / int(videoStat['videoDislikeCount']))
        else:
            lbdDict["likes/dislikes"] = -1
        likesByDislikesBridge.writerow(lbdDict)