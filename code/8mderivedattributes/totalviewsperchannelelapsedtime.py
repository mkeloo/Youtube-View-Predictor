import glob
import csv

fileList = glob.glob('../datasets/8mcsvs/8mmerged/*.csv')
print(fileList)
filelist2 = glob.glob('../datasets/8mcsvs/8mchannelelapsed/*.csv')
print(filelist2)

for file,file2 in zip(fileList,filelist2):
    videoStatFD = open(file,'r')
    videoStatBridge = csv.DictReader(videoStatFD)

    elapsedTimeFD = open(file2)
    elapsedTimeBridge = csv.DictReader(elapsedTimeFD)

    fileName = file.split('/')
    impPart = fileName[4][-6:]
    if impPart[0] == 'a':
        impPart = impPart[-5:]
    print(impPart)

    writeFilePath = '../datasets/8mcsvs/8mtotalviewsbychannelelapsed/totalviewsbychannelelapsedtime'+impPart
    print("Starting "+ writeFilePath)
    commentsbyviewsFD = open(writeFilePath, 'w')
    headers = ("videoId","channelId", "totalviews/channelelapsedtime")
    commentsbyviewsBridge = csv.DictWriter(commentsbyviewsFD, headers)
    commentsbyviewsBridge.writeheader()
    cbvDict = {}
    for videoStat,elapsedTimeStat in zip(videoStatBridge,elapsedTimeBridge):
        cbvDict = {}
        cbvDict["videoId"] = videoStat['videoId']
        cbvDict["channelId"] = videoStat['channelId']
        if elapsedTimeStat['channelelapsedtime'] != 0:
            cbvDict['totalviews/channelelapsedtime'] = float(int(videoStat['channelViewCount']) /
                                                             int(elapsedTimeStat['channelelapsedtime']))
        else:
            cbvDict['totalviews/channelelapsedtime'] = -1
        commentsbyviewsBridge.writerow(cbvDict)
