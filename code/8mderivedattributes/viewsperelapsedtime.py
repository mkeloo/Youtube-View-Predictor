import glob
import csv

fileList = glob.glob('../datasets/8mcsvs/8mmerged/*.csv')
print(fileList)
filelist2 = glob.glob('../datasets/8mcsvs/8melapsedtime/*.csv')
print(filelist2)

for file,file2 in zip(fileList,filelist2):
    videoStatFD = open(file,'r')
    videoStatBridge = csv.DictReader(videoStatFD)

    elapsedTimeFD = open(file2)
    elapsedTimeBridge = csv.DictReader(elapsedTimeFD)

    fileName = file.split('/')
    impPart = fileName[4][-6:]
    if impPart[0] == 'a':
        impPart = impPart[-5:]
    print(impPart)

    writeFilePath = '../datasets/8mcsvs/8mviewsbyelapsedtime/viewsbyelapsedtime'+impPart
    print("Starting "+ writeFilePath)
    commentsbyviewsFD = open(writeFilePath, 'w')
    headers = ("videoId","channelId", "views/elapsedtime")
    commentsbyviewsBridge = csv.DictWriter(commentsbyviewsFD, headers)
    commentsbyviewsBridge.writeheader()
    cbvDict = {}
    for videoStat,elapsedTimeStat in zip(videoStatBridge,elapsedTimeBridge):
        cbvDict = {}
        cbvDict["videoId"] = videoStat['videoId']
        cbvDict["channelId"] = videoStat['channelId']
        if elapsedTimeStat['elapsedtime'] != 0:
            cbvDict['views/elapsedtime'] = float(int(videoStat['videoViewCount']) / int(elapsedTimeStat['elapsedtime']))
        else:
            cbvDict['views/elapsedtime'] = -1
        commentsbyviewsBridge.writerow(cbvDict)
