import glob
import csv

fileList = glob.glob('../../datasets/8mcsvs/8mmerged/*.csv')
print(fileList)

for file in fileList:
    videoStatFD = open(file,'r')
    videoStatBridge = csv.DictReader(videoStatFD)

    fileName = file.split('/')
    impPart = fileName[4][-6:]
    if impPart[0] == 'a':
        impPart = impPart[-5:]
    print(impPart)

    writeFilePath = '../datasets/8mcsvs/8mviewsbysubscriber/viewsbysubscribers' + impPart
    print("Starting " + writeFilePath)
    viewsbysubscribersFD = open(writeFilePath, 'w')
    headers = ("videoId", "views/subscribers")
    viewsbysubscribersBridge = csv.DictWriter(viewsbysubscribersFD, headers)
    viewsbysubscribersBridge.writeheader()

    vbsDict = {}
    for videoStat in videoStatBridge:
        vbsDict = {}
        vbsDict["videoId"] = videoStat['videoId']
        if int(videoStat['subscriberCount']) != 0 and int(videoStat['videoViewCount']) != -1 and \
                        int(videoStat['subscriberCount']) != -1:
            vbsDict["views/subscribers"] = float(int(videoStat['videoViewCount']) / int(videoStat['subscriberCount']))
        else:
            vbsDict["views/subscribers"] = -1
        viewsbysubscribersBridge.writerow(vbsDict)