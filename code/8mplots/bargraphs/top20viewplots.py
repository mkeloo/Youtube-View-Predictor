import csv
import matplotlib.pyplot as plt; plt.rcdefaults()
import matplotlib.pyplot as plt
import numpy as np
import pygal
topfileFD = open('../datasets/8mcsvs/8mtops/top20views.csv', 'r')
topfileBridge = csv.DictReader(topfileFD)

x_axis = []
y_axis = []

feature = "videoViewCount"
for dataPoint in topfileBridge:
    x_axis.append(dataPoint['videoId'])
    y_axis.append(float(dataPoint[feature]))

bar_chart = pygal.Bar()
bar_chart.title = "Top 20 videos by Views"
bar_chart.x_labels = x_axis
bar_chart.add('videoViewCount', y_axis)
bar_chart.render_to_png('../graphs/top20views.png')