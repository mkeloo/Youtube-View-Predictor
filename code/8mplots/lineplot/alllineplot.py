import json
import csv
from pprint import pprint
import pygal


allStatsFD = open('../../datasets/YouTubeDataset_withChannelElapsed.json', 'r')
allStatData = json.load(allStatsFD)

allVideoList = []
keys = []
print(type(allStatData))

for data in allStatData:
    keys = data.keys()
    break


video_list = ['comments/views','dislikes/subscriber', 'elapsedtime', 'views/subscribers',
              'comments/subscriber', 'videoLikeCount', 'dislikes/views', 'videoViewCount',
              'views/elapsedtime', 'VideoCommentCount', 'likes/subscriber', 'likes/views',
              'likes/dislikes', 'videoDislikeCount']
print(keys)


for key in keys:
    print("Starting key: "+ key)
    if key == 'channelPublished' or key == 'videoPublished' or key == 'channelId' or key == 'videoId' \
        or key == 'videoCategoryId' or key == '':
        continue

    req_data = {}
    allVideoList = []
    x_axis = []
    y_axis = []
    for data in allStatData:
        req_data = {}
        req_data['videoId'] = data['videoId']
        req_data['channelId'] = data['channelId']
        req_data[key] = float(data[key])
        allVideoList.append(req_data)

    i = 0
    for point in allVideoList:
        i = i + 1
        if i == 5000:
            if key in video_list:
                x_axis.append(point['videoId'])
            else:
                print(key+" Inside channel")
                x_axis.append(point['channelId'])
            if float(point[key])>=0:
                y_axis.append(float(point[key]))
            i = 0
        else:
            continue
    print(i)
    bar_chart = pygal.Line(show_legend=False)
    bar_chart.title = "Line chart for " + key
    bar_chart.x_labels = x_axis
    bar_chart.add(key, y_axis)

    if '/' in key:
        file_parts = key.split('/')
        file_imp = file_parts[0]+"_by_"+file_parts[1]
    else:
        file_imp = key

    fil_name = '../graphs/linechart_withoutlegend/' + file_imp + '_linechart_withoutLegend.png'
    bar_chart.render_to_png(fil_name)