import json
import csv
from pprint import pprint
import pygal


allStatsFD = open('../../datasets/YouTubeDataset_withChannelElapsed.json', 'r')
allStatData = json.load(allStatsFD)

feature = "channelViewCount"
allVideoList = []
featureList = []
keys = []
print(type(allStatData))

for data in allStatData:
    keys = data.keys()
    break

print(keys)

for key in keys:
    file_imp = ""
    if key == 'channelPublished' or key == 'videoPublished' or key == 'channelId' or key == 'videoId' \
            or key == 'videoCategoryId':
        continue
    print("Starting key: " + key)
    featureList = []
    for data in allStatData:
        if '/' in key:
            file_parts = key.split('/')
            file_imp = file_parts[0] +"_by_"+ file_parts[1]
        else:
            file_imp = key
        if float(data[key])>=0:
            featureList.append(float(data[key]))

    Box_plot = pygal.Box()
    Box_plot.title = "Graph for " + key
    Box_plot.add(key, featureList)
    fil_name = '../graphs/boxplot/' + file_imp + '_boxplot_normal.png'
    Box_plot.render_to_png(fil_name)
