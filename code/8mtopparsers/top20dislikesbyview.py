import json
import csv
from pprint import pprint


allStatsFD = open('../datasets/YouTubeDataset_withChannelElapsed.json', 'r')
allStatData = json.load(allStatsFD)

feature = "dislikes/views"
allVideoList = []

for data in allStatData:
    req_data = {}
    req_data['videoId'] = data['videoId']
    req_data['channelId'] = data['channelId']
    req_data[feature] = float(data[feature])
    allVideoList.append(req_data)

sortedAllVideoList = sorted(allVideoList, key=lambda k:k[feature], reverse=True)
top20DataPoints = sortedAllVideoList[:21]

pprint(top20DataPoints)
print(len(top20DataPoints))

requiredOutputFileFD = open('../datasets/8mcsvs/8mtops/top20dislikesbyviewss.csv', 'w')
headers = ('videoId', 'channelId', feature)
requiredOutputFileBride = csv.DictWriter(requiredOutputFileFD, headers)
requiredOutputFileBride.writeheader()


for component in top20DataPoints:
    requiredOutputFileBride.writerow(component)


print("Finished!!! :D ")